#!/bin/bash
#SBATCH --job-name=filter   
#SBATCH --exclusive             
#SBATCH --export=ALL            
#SBATCH --nodes=1             
#SBATCH --error=filter.err            
#SBATCH --output=filter.out            
#SBATCH --ntasks-per-node=36
#SBATCH --mem=190000            
#SBATCH --time=08:00:00         

#export cwd=`pwd`                                                                
#export PYTHONPATH="$cwd/user_functions":$PYTHONPATH   
e=C201108231751A
p=T045-110s

pypaw-filter_windows -p ./parfile/filter_windows.$p.param.yml -f ./paths/filter.$e.$p.path.json -v
