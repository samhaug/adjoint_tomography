#!/bin/bash
#SBATCH --job-name=measure   
#SBATCH --exclusive             
#SBATCH --export=ALL            
#SBATCH --nodes=1             
#SBATCH --error=measure.err            
#SBATCH --output=measure.out            
#SBATCH --ntasks-per-node=16
#SBATCH --mem=187740
#SBATCH --time=08:00:00         

STARTTIME=$(date +%s)
echo "start time is : $(date +"%T")"

export gen="python ../generate_path_files.py -p ../paths.yml -s ../settings.yml -e ../event_list"

events=`$gen list events`
periods=`$gen list period_bands`

nodes=($(sort $PBS_NODEFILE | uniq | sed 's/:.*//'))
i=0
n_parallel_jobs=${#nodes[@]}

for e in $events
do
    for p in $periods
    do
	    mpiexec -n 16 pypaw-measure_adjoint_asdf \
	    -p ../adjoint/parfile/multitaper.adjoint.$p.config.yml \
	    -f ./paths/measure.$e.$p.path.json \
	    -v 
	    wait
    done
done
wait

ENDTIME=$(date +%s)
Ttaken=$(($ENDTIME - $STARTTIME))
echo
echo "finish time is : $(date +"%T")"
echo "RUNTIME is :  $(($Ttaken / 3600)) hours ::  $(($(($Ttaken%3600))/60)) minutes  :: $(($Ttaken % 60)) seconds."
