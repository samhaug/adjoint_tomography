#!/bin/bash
#SBATCH --job-name=measure   
#SBATCH --exclusive             
#SBATCH --export=ALL            
#SBATCH --nodes=1             
#SBATCH --error=measure.err            
#SBATCH --output=measure.out            
#SBATCH --ntasks-per-node=36
#SBATCH --mem=190000            
#SBATCH --time=08:00:00         

#export cwd=`pwd`                                                                
#export PYTHONPATH="$cwd/user_functions":$PYTHONPATH   
e=C201108231751A
p=T045-110s

mpiexec -n 36 pypaw-measure_adjoint_asdf  -p ../adjoint/parfile/multitaper.adjoint.$p.config.yml -f ./paths/measure.$e.$p.path.json -v 
