#!/bin/bash
#SBATCH --job-name=adjoint   
#SBATCH --exclusive             
#SBATCH --export=ALL            
#SBATCH --nodes=1             
#SBATCH --error=adjoint.err            
#SBATCH --output=adjoint.out            
#SBATCH --ntasks-per-node=36
#SBATCH --mem=190000            
#SBATCH --time=08:00:00         

#export cwd=`pwd`                                                                
#export PYTHONPATH="$cwd/user_functions":$PYTHONPATH   
e=C201108231751A
p=T045-110s

mpiexec -n 2 pypaw-adjoint_asdf \
	    -p ./parfile/multitaper.adjoint.$p.config.yml \
	    -f ./paths/adjoint_mt.all.$e.$p.path.json \
	    -v 
