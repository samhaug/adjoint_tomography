#!/bin/bash
#SBATCH --job-name=adjoint   
#SBATCH --exclusive             
#SBATCH --export=ALL            
#SBATCH --nodes=1             
#SBATCH --error=adjoint.err            
#SBATCH --output=adjoint.out            
#SBATCH --ntasks-per-node=36
#SBATCH --mem=190000            
#SBATCH --time=08:00:00         

STARTTIME=$(date +%s)
echo "start time is : $(date +"%T")"

export gen="python ../generate_path_files.py -p ../paths.yml -s ../settings.yml -e ../event_list"

events=`$gen list events`
periods=`$gen list period_bands`

for e in $events
do
    for p in $periods
    do
	    mpiexec -n 36 pypaw-adjoint_asdf \
	       -p ./parfile/multitaper.adjoint.$p.config.yml \
	       -f ./paths/adjoint_mt.single.$e.$p.path.json \
	       -v &
      wait
      #i=$(($i + 1))
      #if [ $i -ge $n_parallel_jobs ]; then
      #    i=0
      #    wait
      #fi
    done
done
wait

ENDTIME=$(date +%s)
Ttaken=$(($ENDTIME - $STARTTIME))
echo
echo "finish time is : $(date +"%T")"
echo "RUNTIME is :  $(($Ttaken / 3600)) hours ::  $(($(($Ttaken%3600))/60)) minutes  :: $(($Ttaken % 60)) seconds."
