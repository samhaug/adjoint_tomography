#!/bin/bash
#PBS -A GEO111
#PBS -N mt_paired
#PBS -j oe
#PBS -l walltime=6:00:00,nodes=23

if [ -z $PBS_O_WORKDIR ]; then
    PBS_O_WORKDIR=`pwd`
else
    module load python/2.7.9
    source /ccs/proj/geo111/orsvuran/venvs/rhea_py2/bin/activate
fi

if [ -z $PBS_NODEFILE ];  then
    PBS_NODEFILE=`mktemp`
    echo $HOST > $PBS_NODEFILE
fi

STARTTIME=$(date +%s)
echo "start time is : $(date +"%T")"

echo $PBS_O_WORKDIR
cd $PBS_O_WORKDIR

export gen="python ../generate_path_files.py -p ../paths.yml -s ../settings.yml -e ../event_list"

events=`$gen list events`
periods=`$gen list period_bands`

nodes=($(sort $PBS_NODEFILE | uniq | sed 's/:.*//'))
i=0
n_parallel_jobs=${#nodes[@]}
for e in $events
do
    for p in $periods
    do
	mpiexec -n 16 --map-by node -host ${nodes[$i]} pypaw-adjoint_asdf \
	    -p ./parfile/multitaper.adjoint.$p.config.yml \
	    -f ./paths/adjoint_mt.paired.$e.$p.path.json \
	    -v &
	i=$(($i + 1))
	if [ $i -ge $n_parallel_jobs ]; then
	    i=0
	    wait
	fi
    done
done
wait

ENDTIME=$(date +%s)
Ttaken=$(($ENDTIME - $STARTTIME))
echo
echo "finish time is : $(date +"%T")"
echo "RUNTIME is :  $(($Ttaken / 3600)) hours ::  $(($(($Ttaken%3600))/60)) minutes  :: $(($Ttaken % 60)) seconds."
