#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import matplotlib
matplotlib.use("agg")           # NOQA

import pyasdf
import argparse
import matplotlib.pyplot as plt
import numpy as np
import json
from obspy import UTCDateTime

import seaborn as sns
sns.set_style("whitegrid")
sns.set_context("paper")


def load_json(filename):
    with open(filename) as f:
        return json.load(f)


def adjoint_name(station):
    net, sta, loc, comp = station.split(".")
    return "{}_{}_MX{}".format(net, sta, comp[-1])


def get_adjoint_data(ds, station):
    name = adjoint_name(station)
    adj = ds.auxiliary_data.AdjointSources[name]
    data = np.array(adj.data)
    start = 0
    dt = adj.parameters["dt"]
    times = [start+i*dt for i in range(len(data))]
    return times, data


def plot_sum(station, paths):
    inputs = {}
    times = {}
    weights = {}
    for name, data in paths["input_file"].iteritems():
        print(name)
        ds = pyasdf.ASDFDataSet(data["asdf_file"])
        times[name], inputs[name] = get_adjoint_data(ds, station)
        weights[name] = load_json(data["weight_file"])[station]["weight"]

    ds = pyasdf.ASDFDataSet(paths["output_file"])
    otimes, output = get_adjoint_data(ds, station)
    nrows = len(inputs)+1
    fig, axes = plt.subplots(nrows=nrows, figsize=(10, 2*nrows),
                             sharex=True)

    mysum = 0
    for ax, name in zip(axes, inputs):
        ax.plot(times[name], inputs[name], label=name)
        wdata = inputs[name]*weights[name]
        ax.plot(times[name], wdata, label="{} weighted".format(name))
        mysum += wdata
        ax.legend()

    axes[-1].plot(otimes, output, label="Sum")
    axes[-1].plot(otimes, mysum, label="Manual sum")
    axes[-1].legend()
    plt.tight_layout()
    return fig, axes


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Plot misfits as rays")
    parser.add_argument("station_name", help="Station Name")
    parser.add_argument("sum_path_file", help="Path file for summation")
    parser.add_argument("output_file", help="output_file")
    args = parser.parse_args()
    fig, ax = plot_sum(args.station_name, load_json(args.sum_path_file))
    fig.savefig(args.output_file)
