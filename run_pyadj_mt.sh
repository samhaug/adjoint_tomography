#!/bin/bash
#SBATCH --job-name=adjoint
#SBATCH --account=GEO111
#SBATCH --exclusive
#SBATCH --export=ALL
#SBATCH --nodes=23
#SBATCH --time=03:00:00

source ~/.bashrc

STARTTIME=$(date +%s)
echo "start time is : $(date +"%T")"

export gen="python ../generate_path_files.py -p ../paths.yml -s ../settings.yml -e ../event_list"

events=`$gen list events`
periods=`$gen list period_bands`

nodes=(`scontrol show hostnames $SLURM_JOB_NODELIST`)
i=0
j=0
n_parallel_jobs=${#nodes[@]}
echo n_parallel_jobs: $n_parallel_jobs
echo 

for e in $events
do
    for p in $periods
    do
	echo "node: ${nodes[$i]}"
	srun -n16 -N1 -w ${nodes[$i]} \
	     pypaw-adjoint_asdf \
	     -p ./parfile/multitaper.adjoint.$p.config.yml \
	     -f ./paths/adjoint_mt.all.$e.$p.path.json \
	     -v &
	i=$(($i + 1))
	if [ $i -ge $n_parallel_jobs ]; then
	    i=0
	    wait
	    j=$(($j + 1))
	    echo "done: $(($i*$j))"
	fi
    done
done
wait

ENDTIME=$(date +%s)
Ttaken=$(($ENDTIME - $STARTTIME))
echo
echo "finish time is : $(date +"%T")"
echo "RUNTIME is :  $(($Ttaken / 3600)) hours ::  $(($(($Ttaken%3600))/60)) minutes  :: $(($Ttaken % 60)) seconds."

