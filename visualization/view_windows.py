import argparse
'''
Make plots of selected windows
'''
parser = argparse.ArgumentParser()
parser.add_argument("-s","--synt", help="Path to processed synthetic seismograms (h5 format)",type=str)
parser.add_argument("-o","--obsd", help="Path to processed observed seismograms (h5 format)",type=str)
parser.add_argument("-w","--window", help="Path to windows json file",type=str)
parser.add_argument("--synt_tag", help="Synthetic asdf waveform tag",type=str,default='proc_synt')
parser.add_argument("--obsd_tag", help="Observed asdf waveform tag",type=str,default='proc_obsd')
args = parser.parse_args()

import json
from pyasdf import ASDFDataSet
from obspy.core.util.geodetics import gps2DistAzimuth
import obspy
import numpy as np
from matplotlib import pyplot as plt

st = args.synt_tag
ot = args.obsd_tag

dfs = ASDFDataSet(args.synt)
dfo = ASDFDataSet(args.obsd)
j = json.load(open(args.window))


event_label=dfo.events[0].event_descriptions[0].text
evla = round(dfo.events[0]['origins'][0].latitude,2)
evlo = round(dfo.events[0]['origins'][0].longitude,2)
h = round(dfo.events[0]['origins'][0].depth/1000.,1)
mw = round(dfo.events[0]['magnitudes'][0].mag,1)

for ddx,d in enumerate(dfo.waveforms):
    #if ddx==20:
    #    break
    try:
        stla = round(d.StationXML[0][0].latitude,2)
        stlo = round(d.StationXML[0][0].longitude,2)
        gcarc,baz,az = gps2DistAzimuth(stla,stlo,evla,evlo)
        gcarc = round(gcarc/111195.,2)
        az = round(az,2)
        baz = round(baz,2)
        stat = d[ot][0].stats.station
        net = d[ot][0].stats.network
        loc = d[ot][0].stats.location
        obsd_dict = {}
        synt_dict = {}
        win_dict = {}
        for idx,tr in enumerate(d[ot]):
            chan = tr.stats.channel
            win_list = j[net+'.'+stat][net+'.'+stat+'.'+loc+'.'+chan]
            t = np.linspace(0,tr.stats.endtime-tr.stats.starttime,num=tr.stats.npts)
            obsd_dict[chan] = (t,tr.data)
            synt_dict[chan] = (t,dfs.waveforms[net+'_'+stat][st][idx].data)
            win_dict[chan]  = []
            for win in win_list:
                start = win['relative_starttime']
                end = win['relative_endtime']
                win_dict[chan].append([start,end,round(win['dlnA'],3),
                                       round(win['window_weight'],3),
                                       round(win['cc_shift_in_seconds'],3),
                                       round(win['max_cc_value'],3)])

    except KeyError:
        print(bool(obsd_dict))

    if bool(obsd_dict):
        fig,ax_list = plt.subplots(3,1,figsize=(15,10))
        plt.tight_layout()
        title = "{} MW:{} LAT:{} LON:{} H:{}KM {}.{}.{} LAT:{} LON:{} GC:{} AZ:{} BAZ:{}".format(
                event_label,str(mw),str(evla),str(evlo),str(h),net,stat,loc,str(stla),str(stlo),str(gcarc),str(az),str(baz))
        ax_list[0].set_title(title)
        for idx,ax in enumerate(ax_list):
            ax_list[idx].tick_params(labelleft=False)
        for idx,keys in enumerate(obsd_dict):
            ax_list[idx].set_ylabel(keys)
            ax_list[idx].plot(obsd_dict[keys][0],obsd_dict[keys][1],color='k',lw=0.8)
            ax_list[idx].plot(synt_dict[keys][0],synt_dict[keys][1],color='r',lw=0.8)
            ax_list[idx].set_xlim(0,synt_dict[keys][0][-1])
            ycoord=np.max(synt_dict[keys][1])/2.
            for w in win_dict[keys]:
                m = (w[0]+w[1])/2.
                ax_list[idx].axvspan(w[0],w[1],alpha=0.5,color='teal')
                ax_list[idx].axvline(w[0],color='k',lw=0.5)
                ax_list[idx].axvline(w[1],color='k',lw=0.5)
                ax_list[idx].text(m,ycoord,"dlnA:       {}\nweight:    {}\ncc_shft:   {}\nmax_cc:  {}".format(
                                  str(w[2]),str(w[3]),str(w[4]),str(w[5])))
        #plt.savefig("./pics/fig_"+str(ddx)+".pdf")
        plt.show()







