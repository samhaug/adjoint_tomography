import argparse
from sys import exit

'''
Use to hand select dirty waveforms from a record section
'''

parser = argparse.ArgumentParser()
parser.add_argument("-i","--infile", help="Path to input h5 file",type=str)
parser.add_argument("-s","--scale", help="Scale amplitude of traces by multiplier",default=1.0,type=float)
parser.add_argument("-r","--rainbow", help="Add flag to make traces different colors",action="store_true")
parser.add_argument("-c","--component", help="Select component (R/T/Z)",default='T',type=str)
parser.add_argument("-d","--dirty_list", help="Optionally add path to dirty list, for continuation",default='N',type=str)

args = parser.parse_args()

if args.dirty_list == 'N':
    stat_list = []
    net_list = []

else:
    i_f = open(args.dirty_list,'r')
    f = i_f.readlines()[3::]
    net_list = [i.split()[0] for i in f]
    stat_list = [i.split()[1] for i in f]
    i_f.close()

if args.component == 'R':
    comp_id = 0
elif args.component == 'T':
    comp_id = 1
elif args.component == 'Z':
    comp_id = 2
else:
    print("Component must be R, T, or Z")
    exit()

import obspy
from obspy.core.util.geodetics import gps2DistAzimuth
import numpy as np
from matplotlib import pyplot as plt
from pyasdf import ASDFDataSet

remove_list = []
def on_pick(event):
    artist = event.artist
    artist.set_c('white')
    artist.set_alpha(0.0)
    remove_list.append(artist.get_label())
    fig.canvas.draw()
    return

ds = ASDFDataSet(args.infile)
out = ds.events[0].event_descriptions[0].text+"_"+args.component+"_dirty.dat"
ident = args.infile.split('.')[0]
ident = "preprocessed_30s_to_220s"

event = ds.events[0]
origin = event.preferred_origin() or event.origins[0]
event_latitude = origin.latitude
event_longitude = origin.longitude
depth = origin['depth']/1000.
evtime=origin.time

fig,ax = plt.subplots(figsize=(8,18))
ax.set_title("LAT:%3.2f LON:%3.2f H%4.2f km"%(event_latitude,event_longitude,depth))

plt.tight_layout()

for d in ds.waveforms:
    try:
        inv = d.StationXML
        station = d[ident][0].stats.station
        network = d[ident][0].stats.network
        if station in stat_list and network in net_list:
            continue
        station_latitude = inv[0][0].latitude
        station_longitude = inv[0][0].longitude
        gcarc,baz,az = gps2DistAzimuth(station_latitude,station_longitude,
                                       event_latitude,event_longitude)
        #t = np.linspace(0,d[ident][comp_id].stats.endtime-d[ident][comp_id].stats.starttime,num=d[ident][comp_id].stats.npts)
        #data = d[ident][comp_id].data
        gcarc *= 1./111195
        label="%s %s %f %f %f %f %f"%(network,station,station_latitude,station_longitude,gcarc,baz,az)

        o = d[ident][comp_id].stats.starttime-evtime
        length = d[ident][comp_id].stats.endtime-d[ident][comp_id].stats.starttime
        end = o+length
        t = np.linspace(o,end,num=d[ident][comp_id].stats.npts)
        data = d[ident][comp_id].data
        data *= 1./np.max(data)

        if args.rainbow:
            ax.plot(t,gcarc+(data*args.scale),lw=0.7,alpha=0.5,label=label,picker=2)
        else:
            ax.plot(t,gcarc+(data*args.scale),color='k',alpha=0.5,lw=0.7,label=label,picker=2)
    except:
        continue

cid = fig.canvas.mpl_connect('pick_event',on_pick)
ax.set_xlim(0,12000)
plt.show()


if args.dirty_list == 'N':
    f = open(out,'w')
    f.write("evla,evlo\n")
    f.write("%f %f \n"%(event_latitude, event_longitude))
    f.write("net,stat,lat,lon,gcarc,baz,az\n")
    for ii in set(remove_list):
        f.write(ii+'\n')
    f.close()
else:
    f = open(args.dirty_list,'a')
    for ii in set(remove_list):
        f.write(ii+'\n')
    f.close()



