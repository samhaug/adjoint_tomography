import argparse
from sys import exit

'''
Plot record section
'''

parser = argparse.ArgumentParser()
parser.add_argument("-i","--infile", help="Path to input h5 file",type=str)
parser.add_argument("-s","--scale", help="Scale amplitude of traces by multiplier",default=1.0,type=float)
parser.add_argument("-c","--component", help="Select component (R/T/Z)",default='T',type=str)
args = parser.parse_args()

if args.component == 'R':
    comp_id = 0
elif args.component == 'T':
    comp_id = 1
elif args.component == 'Z':
    comp_id = 2
else:
    print("Component must be R, T, or Z")
    exit()

import obspy
from obspy.core.util.geodetics import gps2DistAzimuth
import numpy as np
from matplotlib import pyplot as plt
from pyasdf import ASDFDataSet


ds = ASDFDataSet(args.infile,mode='r')
ident = args.infile.split('.')[0]
if 'observed' in args.infile:
    ident='observed'
else:
    ident = "preprocessed_30s_to_220s"

event = ds.events[0]
origin = event.preferred_origin() or event.origins[0]
event_latitude = origin.latitude
event_longitude = origin.longitude
depth = origin['depth']/1000.
evtime = origin.time

fig,ax = plt.subplots(figsize=(8,18))
ax.set_title("LAT:%3.2f LON:%3.2f H%4.2f km"%(event_latitude,event_longitude,depth))

plt.tight_layout()

for d in ds.waveforms:
    try:
        inv = d.StationXML
        station = d[ident][0].stats.station
        network = d[ident][0].stats.network
        station_latitude = inv[0][0].latitude
        station_longitude = inv[0][0].longitude
        gcarc,baz,az = gps2DistAzimuth(station_latitude,station_longitude,
                                       event_latitude,event_longitude)
        gcarc *= 1./111195
        o = d[ident][comp_id].stats.starttime-evtime
        length = d[ident][comp_id].stats.endtime-d[ident][comp_id].stats.starttime
        end = o+length
        t = np.linspace(o,end,num=d[ident][comp_id].stats.npts)
        data = d[ident][comp_id].data
        data *= 1./np.max(data)
        ax.plot(t,(args.scale*data)+gcarc,color='k',lw=0.7,alpha=0.5)
    except:
        continue

ax.set_xlim(0,12000)
plt.show()



