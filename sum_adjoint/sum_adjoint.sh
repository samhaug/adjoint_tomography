#!/bin/sh
#SBATCH --job-name=sum_adjoint 
#SBATCH --exclusive             
#SBATCH --export=ALL            
#SBATCH --nodes=1             
#SBATCH --error=sum_adjoint.err            
#SBATCH --output=sum_adjoint.out            
#SBATCH --ntasks-per-node=1
#SBATCH --mem=160000            
#SBATCH --time=08:00:00         

STARTTIME=$(date +%s)
echo "start time is : $(date +"%T")"

export gen="python ../generate_path_files.py -p ../paths.yml -s ../settings.yml -e ../event_list"
events=`$gen list events`

for e in $events
do
    pypaw-sum_adjoint_asdf \
    	-p parfile/sum_adjoint.param.yml \
    	-f paths/adjoint_sum.all.$e.path.json \
    	-v &
      wait
    #pypaw-sum_adjoint_asdf \
    #	-p parfile/sum_adjoint.param.yml \
    #	-f paths/adjoint_sum.merged.$e.path.json \
    #	-v &
    #  wait
    # pypaw-sum_adjoint_asdf \
    # 	-p parfile/sum_adjoint.param.yml \
    # 	-f paths/adjoint_sum.dd.$e.path.json \
    # 	-v &
    # pypaw-sum_adjoint_asdf \
    # 	-p parfile/sum_adjoint.param.yml \
    # 	-f paths/adjoint_sum.paired.$e.path.json \
    # 	-v &
    # for p in $periods
    # do
    # 	pypaw-sum_adjoint_asdf \
    # 	    -p parfile/sum_adjoint.param.yml \
    # 	    -f paths/adjoint_sum.all.$e.$p.path.json \
    # 	    -v
    # 	pypaw-sum_adjoint_asdf \
    # 	    -p parfile/sum_adjoint.param.yml \
    # 	    -f paths/adjoint_sum.dd.$e.$p.path.json \
    # 	    -v
    # 	pypaw-sum_adjoint_asdf \
    # 	    -p parfile/sum_adjoint.param.yml \
    # 	    -f paths/adjoint_sum.merged.$e.$p.path.json \
    # 	    -v
    # 	pypaw-sum_adjoint_asdf \
    # 	    -p parfile/sum_adjoint.param.yml \
    # 	    -f paths/adjoint_sum.paired.$e.$p.path.json \
    # 	    -v
    # done

    #i=$(($i + 2))
    #if [ $i -ge $n_parallel_jobs ]; then
    #	i=0
    #	wait
    #fi
done
wait

ENDTIME=$(date +%s)
Ttaken=$(($ENDTIME - $STARTTIME))
echo
echo "finish time is : $(date +"%T")"
echo "RUNTIME is :  $(($Ttaken / 3600)) hours ::  $(($(($Ttaken%3600))/60)) minutes  :: $(($Ttaken % 60)) seconds."
