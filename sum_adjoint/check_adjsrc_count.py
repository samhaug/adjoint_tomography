# -*- coding: utf-8 -*-

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals


import glob
import pyasdf
import sys
from tqdm import tqdm 


def read_stations_file(filename):
    stations = {}
    with open(filename) as f:
        for line in f:
            data = line.split()
            name = "{1}.{0}".format(*data)
            stations[name] = {
                "latitude": float(data[2]),
                "longitude": float(data[3]),
                "elevation": float(data[4]),
                "depth": float(data[5])
            }
    return stations


if len(sys.argv) != 2:
    print("usage: python {} foldername".format(sys.argv[0]))
else:
    folder = sys.argv[1]
    for fname in  tqdm(glob.glob("{}/*/adjoint.h5".format(folder))):
        e = fname.split("/")[-2]
        sta_file = "../station_files/STATIONS_C{}".format(e)
        stations = read_stations_file(sta_file)
        n_sta = len(stations)
        ds = pyasdf.ASDFDataSet(fname)
        n_adj_stas = len(ds.auxiliary_data.AdjointSources)  // 3
        if n_sta != n_adj_stas:
            raise Exception("{} failed. (expected: {}, got: {})".format(fname, n_sta, n_adj_stas))
        else:
            adj_list = ds.auxiliary_data.AdjointSources.list()
            for sta in tqdm(stations):
                for comp in ["MXE", "MXN", "MXZ"]:
                    name = sta.replace(".", "_") + "_" + comp
                    if name not in adj_list:
                        raise Exception("Could not find {} in {}.".format(name, fname))
