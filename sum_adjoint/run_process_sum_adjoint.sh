#!/bin/bash
#SBATCH --job-name=sum_adjoint
#SBATCH --account=GEO111                                                        
#SBATCH --exclusive                                                             
#SBATCH --export=ALL                                                            
#SBATCH --nodes=10                                                       
#SBATCH --time=1:00:00        
#SBATCH --output=sum_adjoint.out       
#SBATCH --error=sum_adjoint.err      


# SAM_H this is the latest script to use as of 9/22/2019

source ~/.bashrc
module restore pypaw
source activate python2

export cwd=`pwd`
export PYTHONPATH="$cwd/user_functions":$PYTHONPATH


STARTTIME=$(date +%s)
echo "start time is : $(date +"%T")"

export gen="python ../generate_path_files.py -p ../paths.yml -s ../settings.yml -e ../event_list"

events=`$gen list events`
periods=`$gen list period_bands`

nodes=(`scontrol show hostnames $SLURM_JOB_NODELIST`)
i=0
j=0
n_parallel_jobs=${#nodes[@]}
echo n_parallel_jobs: $n_parallel_jobs
echo 


for e in $events                                                                
do                                                                              
  echo "node: ${nodes[$i]}"                                                 
  srun -n1 -N1 -w ${nodes[$i]} pypaw-sum_adjoint_asdf -p parfile/sum_adjoint.param.yml -f paths/adjoint_sum.all.$e.path.json -v &
  i=$(($i + 1))                                                             
  if [ $i -ge $n_parallel_jobs ]; then                                      
      i=0                                                                   
      wait                                                                  
      j=$(($j + 1))                                                         
      echo "done: $(($i*$j))"                                               
  fi                                                                        
done                                               

ENDTIME=$(date +%s)
Ttaken=$(($ENDTIME - $STARTTIME))
echo
echo "finish time is : $(date +"%T")"
echo "RUNTIME is :  $(($Ttaken / 3600)) hours ::  $(($(($Ttaken%3600))/60)) minutes  :: $(($Ttaken % 60)) seconds."

