#!/bin/bash
#SBATCH --job-name=sum_adjoint 
#SBATCH --exclusive             
#SBATCH --export=ALL            
#SBATCH --nodes=1             
#SBATCH --error=sum_adjoint.err            
#SBATCH --output=sum_adjoint.out            
#SBATCH --ntasks-per-node=1
#SBATCH --mem=190000            
#SBATCH --time=08:00:00         

#export cwd=`pwd`                                                                
#export PYTHONPATH="$cwd/user_functions":$PYTHONPATH   
e=C201108231751A
p=T045-110s

pypaw-sum_adjoint_asdf \
  -p parfile/sum_adjoint.param.yml \
  -f paths/adjoint_sum.all.$e.path.json \
  -v 

