#!/bin/bash

DIR=output_dd_and_mtrec

for f in ${DIR}/*.h5; do
    eqname=`echo $f | cut -d "/" -f 2 | cut -d "." -f 3 | cut -d "_"  -f 1 | sed "s/^C//g"`
    echo $eqname
    new_folder="${DIR}/${eqname}"
    # echo "mkdir -p $new_folder"
    # echo "mv $f $new_folder/adjoint.h5"
    # echo "mv" ${DIR}/*${eqname}*.json $new_folder
    mkdir -p $new_folder
    mv $f $new_folder/adjoint.h5
    # mv ${DIR}/*${eqname}*.json $new_folder
done
