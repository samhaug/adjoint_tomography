#!/bin/bash
#SBATCH --job-name=fill_zero 
#SBATCH --exclusive             
#SBATCH --export=ALL            
#SBATCH --nodes=1             
#SBATCH --error=fill_zero.err            
#SBATCH --output=fill_zero.out            
#SBATCH --ntasks-per-node=1
#SBATCH --mem=190000            
#SBATCH --time=01:00:00         

STARTTIME=$(date +%s)
echo "start time is : $(date +"%T")"

export gen="python ../generate_path_files.py -p ../paths.yml -s ../settings.yml -e ../event_list"
events=`$gen list events`

for e in $events
do
    python add_missing_stations.py output/adjoint_sum.mt_all.${e}.h5 ../station_files/STATIONS_${e} & wait
done
wait

ENDTIME=$(date +%s)
Ttaken=$(($ENDTIME - $STARTTIME))
echo
echo "finish time is : $(date +"%T")"
echo "RUNTIME is :  $(($Ttaken / 3600)) hours ::  $(($(($Ttaken%3600))/60)) minutes  :: $(($Ttaken % 60)) seconds."
