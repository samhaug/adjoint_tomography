#!/bin/bash
#SBATCH --job-name=fill_zero
#SBATCH --account=GEO111                                                        
#SBATCH --exclusive                                                             
#SBATCH --export=ALL                                                            
#SBATCH --nodes=10                                                       
#SBATCH --time=1:00:00        
#SBATCH --output=fill_zero.out       
#SBATCH --error=fill_zero.err      

STARTTIME=$(date +%s)
echo "start time is : $(date +"%T")"

export gen="python ../generate_path_files.py -p ../paths.yml -s ../settings.yml -e ../event_list"
events=`$gen list events`

for e in $events
do
  srun -n1 -N1 -w ${nodes[$i]} python add_missing_stations.py $SCRATCH/output_sum_adjoint/adjoint_sum.mt_all.${e}.h5 ../station_files/STATIONS_${e} & 
  i=$(($i + 1))                                                             
  if [ $i -ge $n_parallel_jobs ]; then                                      
      i=0                                                                   
      wait                                                                  
      j=$(($j + 1))                                                         
      echo "done: $(($i*$j))"                                               
  fi                                                                        
done
wait

ENDTIME=$(date +%s)
Ttaken=$(($ENDTIME - $STARTTIME))
echo
echo "finish time is : $(date +"%T")"
echo "RUNTIME is :  $(($Ttaken / 3600)) hours ::  $(($(($Ttaken%3600))/60)) minutes  :: $(($Ttaken % 60)) seconds."
