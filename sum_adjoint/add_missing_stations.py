#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import argparse
import pyasdf
import json
import numpy as np
from tqdm import tqdm

def find_station(name, stations):
    for station in stations:
        if name.startswith(station):
            return stations[station]


def add_missing_stations(ds, stations):
    all_adjoints = ds.auxiliary_data.AdjointSources.list()
    adjoint_stations = set(["_".join(x.split("_")[:2]) for x in all_adjoints])
    station_names = set()
    for station in stations:
        net, name = station.split(".")
        station_names.add("_".join([net, name]))
    missing = station_names.difference(adjoint_stations)
    sample = ds.auxiliary_data.AdjointSources[all_adjoints[0]]
    params = sample.parameters
    npts = len(sample.data)
    zero = np.zeros(npts, dtype=sample.data.dtype)
    print("Found {} missing stations".format(len(missing)))
    for st in tqdm(missing):
        station, network = st.split("_")
        for comp in ["MXZ", "MXE", "MXN"]:
            loc = find_station(st.replace("_", "."), stations)
            new_params = {
                "adjoint_source_type": params["adjoint_source_type"],
                "component": comp,
                "depth_in_m": loc["depth"],
                "latitude": loc["latitude"],
                "longitude": loc["longitude"],
                "elevation_in_m": loc["elevation"],
                "dt": params["dt"],
                "location": "",
                "max_period": params["max_period"],
                "min_period": params["min_period"],
                "misfit": 0.0,
                "starttime": params["starttime"],
                "station_id": st.replace("_", "."),
                "units": "m"
            }
            ds.add_auxiliary_data(data=zero,
                                 data_type="AdjointSources",
                                 path=st+"_"+comp,
                                 parameters=new_params)



def read_stations_file(filename):
    stations = {}
    with open(filename) as f:
        for line in f:
            data = line.split()
            name = "{1}.{0}".format(*data)
            stations[name] = {
                "latitude": float(data[2]),
                "longitude": float(data[3]),
                "elevation": float(data[4]),
                "depth": float(data[5])
            }
    return stations


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Extract station information from asdf file as json")
    parser.add_argument('input_asdf')
    parser.add_argument('stations_file')
    args = parser.parse_args()

    ds = pyasdf.ASDFDataSet(args.input_asdf)
    stations = read_stations_file(args.stations_file)

    add_missing_stations(ds, stations)
