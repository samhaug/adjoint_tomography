#!/bin/bash


# pypaw-process_asdf \
#     -p ./parfile/proc_obsd.${2}.param.yml \
#     -f ./paths/proc_obsd.${1}.independent_sc.${2}.path.json \
#     -v

pypaw-process_asdf \
    -p ./parfile/proc_synt.${2}.param.yml \
    -f ./paths/proc_synt.${1}.independent.M25_1DQ.${2}.path.json \
    -v
