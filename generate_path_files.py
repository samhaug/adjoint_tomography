#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Generate path files for pypaw

:copyright:
   Ridvan Orsvuran (orsvuran@geoazur.unice.fr), 2017
:license:
    GNU Lesser General Public License, version 3 (LGPLv3)
    (http://www.gnu.org/licenses/lgpl-3.0.en.html)
"""

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import argparse
import os
import json
import yaml

from pprint import pprint


def load_json(filename):
    with open(filename) as f:
        data = json.load(f)
    return data


def dump_json(data, filename):
    with open(filename, "w") as file_:
        json.dump(data, file_, indent=2, sort_keys=True)
    print("{} is written.".format(filename))


def load_yaml(filename):
    with open(filename) as f:
        data = yaml.load(f,Loader=yaml.FullLoader)
    return data


def dump_yaml(data, filename):
    with open(filename, "w") as f:
        data = yaml.dump(data, f, indent=2)
    print("{} is written.".format(filename))
    return data


def load_events(filename):
    with open(filename) as f:
        events = [line.replace("\n", "") for line in f
                  if not line.startswith("#")]
    return events


def parse_folder(folder, parent):
    name, data = folder.items()[0]
    paths = {}
    files = {}
    if isinstance(data, str):
        path = data
        subfolders = []
        child_files = []
    else:
        path = data.get("path", name)
        subfolders = data.get("subfolders", [])
        child_files = data.get("files", [])

    paths[name] = os.path.join(parent, path)

    for child_file in child_files:
        fname, filepath = child_file.items()[0]
        files[fname] = os.path.join(paths[name], filepath)

    for subfolder in subfolders:
        sub_paths, sub_files = parse_folder(subfolder,
                                            paths[name])
        paths.update(sub_paths)
        files.update(sub_files)

    return paths, files


def load_path_config(filename):
    data = load_yaml(filename)
    root = os.path.dirname(os.path.abspath(filename))
    folders = {}
    files = {}
    for subfolder in data:
        subfolders, subfiles = parse_folder(subfolder,
                                            parent=root)
        folders.update(subfolders)
        files.update(subfiles)
    return folders, files


def makedir(dirname):
    try:
        os.makedirs(dirname)
    except:
        pass


def f(name, eventname=None, period_band=None):
    return files[name].format(eventname=eventname,
                              period_band=period_band)


def d(name, eventname=None, period_band=None):
    return folders[name].format(eventname=eventname,
                                period_band=period_band)


def list_events():
    for eventname in events:
        print(eventname)


def list_period_bands():
    for period_band in settings["period_bands"]:
        print(period_band)


def generate_folders():
    for eventname in events:
        for period_band in settings["period_bands"]:
            for name, path in folders.iteritems():
                makedir(path.format(eventname=eventname,
                                    period_band=period_band))


def generate_converter():
    for eventname in events:
        for seis_type in ["obsd", "synt"]:
            data = {
                "filetype": "sac",
                "output_file": f("raw_"+seis_type, eventname),
                "tag": settings["raw_{}_tag".format(seis_type)],
                "quakeml_file": f("quakeml", eventname),
                "staxml_dir": d("staxml", eventname),
                "waveform_dir": d("{}_sac".format(seis_type), eventname)
            }
            dump_json(data,
                      f("{}_converter_path".format(seis_type), eventname))


def generate_proc():
    for eventname in events:
        for period_band in settings["period_bands"]:
            for seis_type in ["obsd", "synt"]:
                data = {
                    "input_asdf": f("raw_"+seis_type, eventname),
                    "input_tag": settings["raw_{}_tag".format(seis_type)],
                    "output_asdf": f("proc_"+seis_type,
                                     eventname, period_band),
                    "output_tag": settings["proc_{}_tag".format(seis_type)]
                }
                dump_json(data, f("{}_proc_path".format(seis_type),
                                  eventname, period_band))


def generate_windows():
    for eventname in events:
        for period_band in settings["period_bands"]:
            data = {
                "figure_mode": True,
                "obsd_asdf": f("proc_obsd", eventname, period_band),
                "obsd_tag": settings["proc_obsd_tag"],
                "output_file": f("windows_file", eventname, period_band),
                "synt_asdf": f("proc_synt", eventname, period_band),
                "synt_tag": settings["proc_synt_tag"]
            }
            dump_json(data,
                      f("window_path", eventname, period_band))


def generate_measure():
    for eventname in events:
        for period_band in settings["period_bands"]:
            data = {
                "figure_dir": d("measure_figures"),
                "figure_mode": False,
                "obsd_asdf": f("proc_obsd", eventname, period_band),
                "obsd_tag": settings["proc_obsd_tag"],
                "output_file": f("measure_file", eventname, period_band),
                "synt_asdf": f("proc_synt", eventname, period_band),
                "synt_tag": settings["proc_synt_tag"],
                "window_file": f("windows_file", eventname, period_band)
            }
            dump_json(data,
                      f("measure_path", eventname, period_band))


def generate_stations():
    for eventname in events:
        data = {
            "input_asdf": f("proc_obsd", eventname, settings["period_bands"][0]),
            "outputfile": f("stations_file", eventname),
        }
        dump_json(data,
                  f("stations_path", eventname))


def generate_filter():
    for eventname in events:
        for period_band in settings["period_bands"]:
            data = {
                "measurement_file": f("measure_file", eventname, period_band),
                "output_file": f("windows_filter_file", eventname, period_band),
                "station_file": f("stations_file", eventname),
                "window_file": f("windows_file", eventname, period_band)
            }
            dump_json(data,
                      f("filter_path", eventname, period_band))


def generate_adjoint():
    for eventname in events:
        for period_band in settings["period_bands"]:
            data = {
                "figure_dir": d("adjoint_figures"),
                "figure_mode": False,
                "obsd_asdf": f("proc_obsd", eventname, period_band),
                "obsd_tag": settings["proc_obsd_tag"],
                "output_file": f("adjoint_file", eventname, period_band),
                "synt_asdf": f("proc_synt", eventname, period_band),
                "synt_tag": settings["proc_synt_tag"],
                "window_file": f("windows_filter_file", eventname, period_band)
            }
            dump_json(data,
                      f("adjoint_path", eventname, period_band))


def count_windows(eventname, windows_filename):
    measurements = {}
    for period_band in settings["period_bands"]:
        windows_file = f(windows_filename, eventname, period_band)
        windows_data = load_json(windows_file)
        #SAM
        measurements[period_band] = dict((x, 0) for x in ["BHR", "BHT", "BHZ"])
        #measurements[period_band] = dict((x, 0) for x in ["MXR", "MXT", "MXZ"])
        for station, components in windows_data.iteritems():
            for component, windows in components.iteritems():
                c = component.split(".")[-1]
                measurements[period_band][c] += len(windows)
    return measurements


def get_ratio(eventname, windows_filename):
    counts = count_windows(eventname, windows_filename)
    for p in counts:
        for c in counts[p]:
            if counts[p][c] != 0:
                counts[p][c] = 1 / counts[p][c]
            else:
                counts[p][c] = 0
    return counts


def generate_weight_params():
    template = load_yaml(f("weight_template"))
    for eventname in events:
        ratio = get_ratio(eventname, "windows_filter_file")
        data = template.copy()
        data["category_weighting"]["ratio"] = ratio
        dump_yaml(data, f("weight_parfile", eventname))


def generate_weight_paths():
    for eventname in events:
        data = {"input": {},
                "logfile": f("weight_log", eventname)}
        for period_band in settings["period_bands"]:
            data["input"][period_band] = {
                "asdf_file": f("proc_synt", eventname, period_band),
                "output_file": f("weight_file", eventname, period_band),
                "station_file": f("stations_file", eventname),
                "window_file": f("windows_filter_file", eventname, period_band)
            }
        dump_json(data, f("weight_path", eventname))


def generate_sum():
    for eventname in events:
        data = {"input_file": {},
                "output_file": f("sum_adjoint_file", eventname)}
        for period_band in settings["period_bands"]:
            data["input_file"][period_band] = {
                "asdf_file": f("adjoint_file", eventname, period_band),
                "weight_file": f("weight_file", eventname, period_band),
            }
        dump_json(data, f("sum_adjoint_path", eventname))


def generate_dd():
    for eventname in events:
        for period_band in settings["period_bands"]:
            data = {
                "obsd_asdf": f("proc_obsd", eventname, period_band),
                "obsd_tag": settings["proc_obsd_tag"],
                "windows_file": f("windows_filter_file", eventname, period_band),  # NOQA
                "output": f("dd_pairs", eventname, period_band),
                "figure_mode": False,
                "figure_dir": d("dd_figures"),
                "stations_file": f("stations_file", eventname)
            }
            dump_json(data, f("dd_path", eventname, period_band))


def generate_dd_filter_win_script():
    script = "#!/bin/bash\n\n"
    for eventname in events:
        for period_band in settings["period_bands"]:
            script += "pypaw-dd_filter_paired_windows {} {}\n".format(
                f("windows_filter_file", eventname, period_band),
                f("dd_pairs_filter", eventname, period_band))

    with open(f("dd_filter_script"), "w") as sf:
        sf.write(script)


def generate_dd_adjoint():
    for eventname in events:
        for period_band in settings["period_bands"]:
            data = {
                "figure_dir": d("adjoint_figures"),
                "figure_mode": False,
                "obsd_asdf": f("proc_obsd", eventname, period_band),
                "obsd_tag": settings["proc_obsd_tag"],
                "output_file": f("dd_adjoint_file", eventname, period_band),
                "pair_file": f("dd_pairs", eventname, period_band),
                "synt_asdf": f("proc_synt", eventname, period_band),
                "synt_tag": settings["proc_synt_tag"],
                "window_file": f("windows_filter_file", eventname, period_band)
            }
            dump_json(data, f("dd_adjoint_path", eventname, period_band))


def generate_dd_measure():
    for eventname in events:
        for period_band in settings["period_bands"]:
            data = {
                "obsd_asdf": f("proc_obsd", eventname, period_band),
                "obsd_tag": settings["proc_obsd_tag"],
                "output_file": f("dd_measure_file", eventname, period_band),
                "pair_file": f("dd_pairs", eventname, period_band),
                "synt_asdf": f("proc_synt", eventname, period_band),
                "synt_tag": settings["proc_synt_tag"],
                "window_file": f("windows_filter_file", eventname, period_band)
            }
            dump_json(data, f("dd_measure_path", eventname, period_band))


def generate_adjoint_single():
    for eventname in events:
        for period_band in settings["period_bands"]:
            data = {
                "figure_dir": d("adjoint_figures"),
                "figure_mode": False,
                "obsd_asdf": f("proc_obsd", eventname, period_band),
                "obsd_tag": settings["proc_obsd_tag"],
                "output_file": f("adjoint_single_file", eventname, period_band),
                "synt_asdf": f("proc_synt", eventname, period_band),
                "synt_tag": settings["proc_synt_tag"],
                "window_file": f("windows_single_file", eventname, period_band)
            }
            dump_json(data,
                      f("adjoint_single_path", eventname, period_band))


def generate_adjoint_paired():
    for eventname in events:
        for period_band in settings["period_bands"]:
            data = {
                "figure_dir": d("adjoint_figures"),
                "figure_mode": False,
                "obsd_asdf": f("proc_obsd", eventname, period_band),
                "obsd_tag": settings["proc_obsd_tag"],
                "output_file": f("adjoint_paired_file", eventname, period_band),
                "synt_asdf": f("proc_synt", eventname, period_band),
                "synt_tag": settings["proc_synt_tag"],
                "window_file": f("windows_paired_file", eventname, period_band)
            }
            dump_json(data,
                      f("adjoint_paired_path", eventname, period_band))


def generate_merge_adjoint():
    for eventname in events:
        for period_band in settings["period_bands"]:
            data = {
                "single_adj": f("adjoint_single_file", eventname, period_band),
                "dd_adj": f("dd_adjoint_file", eventname, period_band),
                "single_weight": 1.0,
                "dd_weight": 1.0,
                "pair_file": f("dd_pairs_filter", eventname, period_band),
                "output_file": f("merged_adjoint_file", eventname, period_band),  # NOQA
                "single_window_file": f("windows_single_file", eventname, period_band)
            }
            dump_json(data,
                      f("merge_path", eventname, period_band))


def generate_adjoint_ddonly():
    for eventname in events:
        for period_band in settings["period_bands"]:
            data = {
                "single_adj": f("empty_ds", eventname, period_band),
                "dd_adj": f("dd_adjoint_file", eventname, period_band),
                "single_weight": 0.0,
                "dd_weight": 1.0,
                "pair_file": f("dd_pairs_filter", eventname, period_band),
                "output_file": f("ddonly_adjoint_file", eventname, period_band),  # NOQA
                "single_window_file": f("empty_windows", eventname, period_band)
            }
            dump_json(data,
                      f("merge_ddonly_path", eventname, period_band))


def generate_weight_paired_params():
    template = load_yaml(f("weight_template"))
    for eventname in events:
        ratio = get_ratio(eventname, "windows_paired_file")
        data = template.copy()
        data["category_weighting"]["ratio"] = ratio
        dump_yaml(data, f("weight_paired_parfile", eventname))


def generate_rec_weight_params():
    template = load_yaml(f("weight_rec_template"))
    for eventname in events:
        ratio = get_ratio(eventname, "windows_filter_file")
        data = template.copy()
        data["category_weighting"]["ratio"] = ratio
        dump_yaml(data, f("weight_rec_parfile", eventname))


def generate_rec_weight_paired_params():
    template = load_yaml(f("weight_rec_template"))
    for eventname in events:
        ratio = get_ratio(eventname, "windows_paired_file")
        data = template.copy()
        data["category_weighting"]["ratio"] = ratio
        dump_yaml(data, f("weight_rec_paired_parfile", eventname))


def generate_weight_paired_paths():
    for eventname in events:
        data = {"input": {},
                "logfile": f("weight_log", eventname)}
        for period_band in settings["period_bands"]:
            data["input"][period_band] = {
                "asdf_file": f("proc_synt", eventname, period_band),
                "output_file": f("weight_paired_file", eventname, period_band),
                "station_file": f("stations_file", eventname),
                "window_file": f("windows_paired_file", eventname, period_band)
            }
        dump_json(data, f("weight_paired_path", eventname))


def generate_rec_weight_paths():
    for eventname in events:
        data = {"input": {},
                "logfile": f("rec_weight_log", eventname)}
        for period_band in settings["period_bands"]:
            data["input"][period_band] = {
                "asdf_file": f("proc_synt", eventname, period_band),
                "output_file": f("rec_weight_file", eventname, period_band),
                "station_file": f("stations_file", eventname),
                "window_file": f("windows_filter_file", eventname, period_band)
            }
        dump_json(data, f("rec_weight_path", eventname))


def generate_rec_weight_paired_paths():
    for eventname in events:
        data = {"input": {},
                "logfile": f("rec_weight_log", eventname)}
        for period_band in settings["period_bands"]:
            data["input"][period_band] = {
                "asdf_file": f("proc_synt", eventname, period_band),
                "output_file": f("rec_weight_paired_file", eventname, period_band),
                "station_file": f("stations_file", eventname),
                "window_file": f("windows_paired_file", eventname, period_band)
            }
        dump_json(data, f("rec_weight_paired_path", eventname))


def generate_count_all_windows():
    for eventname in events:
        print("Event: {}".format(eventname))
        counts = count_windows(eventname, "windows_filter_file")
        pprint(counts)


def generate_count_paired_windows():
    for eventname in events:
        print("Event: {}".format(eventname))
        counts = count_windows(eventname, "windows_paired_file")
        pprint(counts)


def generate_ind_weight_paths():
    for eventname in events:
        ratios = get_ratio(eventname, "windows_filter_file")
        for period_band in ratios.keys():
            all_data = {
                "input": {
                    period_band: {
                        "asdf_file": f("proc_synt", eventname, period_band),
                        "output_file": f("weight_ind_all_file", eventname, period_band),
                        "station_file": f("stations_file", eventname),
                        "window_file": f("windows_filter_file", eventname, period_band)
                    }},
                "logfile": f("weight_log", eventname, period_band)}
            paired_data = {
                "input": {
                    period_band: {
                        "asdf_file": f("proc_synt", eventname, period_band),
                        "output_file": f("weight_ind_paired_file", eventname, period_band),
                        "station_file": f("stations_file", eventname),
                        "window_file": f("windows_paired_file", eventname, period_band)
                    }},
                "logfile": f("weight_log", eventname, period_band)}
            dump_json(all_data, f("weight_ind_all_path", eventname, period_band))
            dump_json(paired_data, f("weight_ind_paired_path", eventname, period_band))


def generate_ind_weight_params():
    template = load_yaml(f("weight_template"))
    for eventname in events:
        all_ratios = get_ratio(eventname, "windows_filter_file")
        paired_ratios = get_ratio(eventname, "windows_paired_file")
        for period_band in settings["period_bands"]:
            data = template.copy()
            ratio = {period_band: all_ratios[period_band]}
            data["category_weighting"]["ratio"] = ratio
            dump_yaml(data, f("weight_ind_all_parfile", eventname, period_band))
            data = template.copy()
            ratio = {period_band: paired_ratios[period_band]}
            data["category_weighting"]["ratio"] = ratio
            dump_yaml(data, f("weight_ind_paired_parfile", eventname, period_band))


def generate_weights_all():
    generate_weight_params()
    generate_weight_paths()
    generate_weight_paired_params()
    generate_weight_paired_paths()
    generate_rec_weight_params()
    generate_rec_weight_paths()
    generate_rec_weight_paired_params()
    generate_rec_weight_paired_paths()
    generate_ind_weight_params()
    generate_ind_weight_paths()


def generate_sum_merged():
    for eventname in events:
        data = {"input_file": {},
                "output_file": f("sum_adjoint_merged_file", eventname)}
        for period_band in settings["period_bands"]:
            data["input_file"][period_band] = {
                "asdf_file": f("merged_adjoint_file", eventname, period_band),
                "weight_file": f("weight_file", eventname, period_band),
            }
        dump_json(data, f("sum_adjoint_merged_path", eventname))


def generate_sum_all_individuals():
    for eventname in events:
        for period_band in settings["period_bands"]:
            data = {"input_file": {},
                    "output_file": f("sum_adjoint_ind_file", eventname, period_band)}
            data["input_file"][period_band] = {
                "asdf_file": f("adjoint_file", eventname, period_band),
                "weight_file": f("weight_ind_all_file", eventname, period_band),
            }
            dump_json(data, f("sum_adjoint_ind_path", eventname, period_band))
            data = {"input_file": {},
                    "output_file": f("sum_adjoint_ind_merged_file", eventname, period_band)}
            data["input_file"][period_band] = {
                "asdf_file": f("merged_adjoint_file", eventname, period_band),
                "weight_file": f("weight_ind_all_file", eventname, period_band),
            }
            dump_json(data, f("sum_adjoint_ind_merged_path", eventname, period_band))


def generate_sum_paired():
    for eventname in events:
        data = {"input_file": {},
                "output_file": f("sum_adjoint_paired_file", eventname)}
        for period_band in settings["period_bands"]:
            data["input_file"][period_band] = {
                "asdf_file": f("adjoint_paired_file", eventname, period_band),
                "weight_file": f("weight_paired_file", eventname, period_band),
            }
        dump_json(data, f("sum_adjoint_paired_path", eventname))


def generate_sum_dd_and_mt():
    for eventname in events:
        data = {"input_file": {},
                "output_file": f("sum_adjoint_dd_and_mt_file", eventname)}
        for period_band in settings["period_bands"]:
            data["input_file"][period_band+"_DD"] = {
                "asdf_file": f("ddonly_adjoint_file", eventname, period_band),
                "weight_file": f("weight_paired_file", eventname, period_band),
            }
            data["input_file"][period_band] = {
                "asdf_file": f("adjoint_file", eventname, period_band),
                "weight_file": f("weight_file", eventname, period_band),
            }
        dump_json(data, f("sum_adjoint_dd_and_mt_path", eventname))


def generate_sum_dd_and_mtrec():
    for eventname in events:
        data = {"input_file": {},
                "output_file": f("sum_adjoint_dd_and_mtrec_file", eventname)}
        for period_band in settings["period_bands"]:
            data["input_file"][period_band+"_DD"] = {
                "asdf_file": f("ddonly_adjoint_file", eventname, period_band),
                "weight_file": f("weight_paired_file", eventname, period_band),
            }
            data["input_file"][period_band] = {
                "asdf_file": f("adjoint_file", eventname, period_band),
                "weight_file": f("rec_weight_file", eventname, period_band),
            }
        dump_json(data, f("sum_adjoint_dd_and_mtrec_path", eventname))


def generate_sum_dd():
    for eventname in events:
        data = {"input_file": {},
                "output_file": f("sum_adjoint_dd_file", eventname)}
        for period_band in settings["period_bands"]:
            data["input_file"][period_band] = {
                "asdf_file": f("dd_adjoint_file", eventname, period_band),
                "weight_file": f("weight_paired_file", eventname, period_band),
            }
        dump_json(data, f("sum_adjoint_dd_path", eventname))


def generate_sum_paired_individuals():
    for eventname in events:
        for period_band in settings["period_bands"]:
            data = {"input_file": {},
                    "output_file": f("sum_adjoint_ind_paired_file", eventname, period_band)}
            data["input_file"][period_band] = {
                "asdf_file": f("adjoint_paired_file", eventname, period_band),
                "weight_file": f("weight_ind_paired_file", eventname, period_band),
            }
            dump_json(data, f("sum_adjoint_ind_paired_path", eventname, period_band))
            data = {"input_file": {},
                    "output_file": f("sum_adjoint_ind_dd_file", eventname, period_band)}
            data["input_file"][period_band] = {
                "asdf_file": f("dd_adjoint_file", eventname, period_band),
                "weight_file": f("weight_ind_paired_file", eventname, period_band),
            }
            dump_json(data, f("sum_adjoint_ind_dd_path", eventname, period_band))


def generate_sum_with_rec():
    for eventname in events:
        data = {"input_file": {},
                "output_file": f("sum_adjoint_file_with_rec", eventname)}
        for period_band in settings["period_bands"]:
            data["input_file"][period_band] = {
                "asdf_file": f("adjoint_file", eventname, period_band),
                "weight_file": f("rec_weight_file", eventname, period_band),
            }
        dump_json(data, f("sum_adjoint_path_with_rec", eventname))


def generate_sum_merged_with_rec():
    for eventname in events:
        data = {"input_file": {},
                "output_file": f("sum_adjoint_merged_file_with_rec", eventname)}
        for period_band in settings["period_bands"]:
            data["input_file"][period_band] = {
                "asdf_file": f("merged_adjoint_file", eventname, period_band),
                "weight_file": f("rec_weight_file", eventname, period_band),
            }
        dump_json(data, f("sum_adjoint_merged_path_with_rec", eventname))


def generate_sum_paired_with_rec():
    for eventname in events:
        data = {"input_file": {},
                "output_file": f("sum_adjoint_paired_file_with_rec", eventname)}
        for period_band in settings["period_bands"]:
            data["input_file"][period_band] = {
                "asdf_file": f("adjoint_paired_file", eventname, period_band),
                "weight_file": f("rec_weight_paired_file", eventname, period_band),
            }
        dump_json(data, f("sum_adjoint_paired_path_with_rec", eventname))


def generate_sum_dd_with_rec():
    for eventname in events:
        data = {"input_file": {},
                "output_file": f("sum_adjoint_dd_file_with_rec", eventname)}
        for period_band in settings["period_bands"]:
            data["input_file"][period_band] = {
                "asdf_file": f("dd_adjoint_file", eventname, period_band),
                "weight_file": f("rec_weight_paired_file", eventname, period_band),
            }
        dump_json(data, f("sum_adjoint_dd_path_with_rec", eventname))


def generate_sum_all():
    generate_sum()
    generate_sum_merged()
    #generate_sum_paired()
    #generate_sum_dd()
    #generate_sum_all_individuals()
    #generate_sum_paired_individuals()
    generate_sum_with_rec()
    generate_sum_merged_with_rec()
    #generate_sum_paired_with_rec()
    #generate_sum_dd_with_rec()


def generate_source_weights():
    import pyasdf
    from tqdm import tqdm
    from pytomo3d.source.source_weights import calculate_source_weights
    data = {}
    for eventname in tqdm(events):
        counts = count_windows(eventname, "windows_filter_file")
        total_count = 0
        for p in counts:
            for c in counts[p]:
                total_count += counts[p][c]
        ds = pyasdf.ASDFDataSet(f("raw_synt", eventname))
        data[eventname] = {
            "source": ds.events,
            "window_counts": total_count
        }
    params = {"flag": True, "plot": True, "search_ratio": 0.8}
    output_file = "./source_weights"
    calculate_source_weights(data, params, output_file, _verbose=True)


def generate_source_locations():
    import pyasdf
    from tqdm import tqdm
    data = {}
    for eventname in tqdm(events):
        ds = pyasdf.ASDFDataSet(f("raw_synt", eventname))
        origin = ds.events[0].preferred_origin()
        data[eventname] = {
            "latitude": origin.latitude,
            "longitude": origin.longitude
        }
    dump_json(data, "sources.json")


def generate_plot_source_weights():
    import cartopy.crs as ccrs
    import cartopy.feature as cfeature
    import matplotlib.pyplot as plt
    projection = ccrs.Robinson()
    ax = plt.axes(projection=projection)
    ax.coastlines()
    ax.add_feature(cfeature.LAND)
    ax.set_global()
    fig = plt.gcf()
    source_locs = load_json("sources.json")
    weights = {}
    with open("source_weights") as f:
        for line in f:
            name, weight = line.split()
            weights[name] = float(weight)

    lons = []
    lats = []
    ws = []
    for name in weights:
        lons.append(source_locs[name]["longitude"])
        lats.append(source_locs[name]["latitude"])
        ws.append(weights[name])
    ws = [w/max(ws) for w in ws]
    sc = ax.scatter(lons, lats, 50, c=ws, marker="*",
                    transform=ccrs.Geodetic())
    cbar = plt.colorbar(sc, fraction=0.024, pad=0.02, ax=ax)
    cbar.set_label("Source Weights (Normalized)")
    plt.tight_layout()
    fig.savefig("source_weights.pdf")


def check_data(data):
    for name, value in data.iteritems():
        if isinstance(value, unicode) and value.startswith("/") and "output" not in name:
            if not os.path.exists(value):
                raise Exception("{} does not exists: {}".format(
                    name, value))
            else:
                print("{} exists.".format(value))
        elif isinstance(value, dict):
            check_data(value)
        else:
            print("{} ({}) is skipped.".format(value, name))


def check_files(*filenames):
    for filename in filenames:
        data = load_json(filename)
        check_data(data)
    print("Check completed")


if __name__ == '__main__':
    steps = dict([(x[9:], x) for x in locals().keys()
                  if x.startswith('generate_')])
    list_funcs = dict([(x[5:], x) for x in locals().keys()
                       if x.startswith('list_')])
    parser = argparse.ArgumentParser(
        description='Generate path files')
    parser.add_argument('-p', '--paths-file', default="paths.yml")
    parser.add_argument('-s', '--settings-file', default="settings.yml")
    parser.add_argument('-e', '--events-file', default="event_list")
    subparsers = parser.add_subparsers(title="commands", dest="command")

    parser_generate = subparsers.add_parser('generate')
    parser_generate.add_argument("step",  help="file to generate",
                                 choices=steps.keys())

    parser_filename = subparsers.add_parser("filename")
    parser_filename.add_argument("name")
    parser_filename.add_argument("eventname")
    parser_filename.add_argument("period_band")

    parser_filename = subparsers.add_parser("folder")
    parser_filename.add_argument("name")
    parser_filename.add_argument("eventname")
    parser_filename.add_argument("period_band")

    parser_list = subparsers.add_parser("list")
    parser_list.add_argument("name",  help="name to list",
                             choices=list_funcs.keys())


    parser_list = subparsers.add_parser("check")
    parser_list.add_argument("filenames",  help="pathfiles to check",
                             nargs="*")

    args = parser.parse_args()
    folders, files = load_path_config(args.paths_file)
    settings = load_yaml(args.settings_file)
    events = load_events(args.events_file)

    if args.command  == "generate":
        locals()[steps[args.step]]()
    elif args.command == "filename":
        print(f(args.name, args.eventname, args.period_band))
    elif args.command == "folder":
        print(d(args.name, args.eventname, args.period_band))
    elif args.command == "list":
        locals()[list_funcs[args.name]]()
    elif args.command == "list_period_bands":
        list_period_bands()
    elif args.command == "check":
        check_files(*args.filenames)
