#!/bin/bash
#SBATCH --job-name=process      
#SBATCH --exclusive             
#SBATCH --export=ALL            
#SBATCH --nodes=2             
#SBATCH --error=process.err            
#SBATCH --output=process.out            
#SBATCH --ntasks-per-node=36
#SBATCH --mem=190000            
#SBATCH --time=02:00:00         

mpiexec -np 72 python process_parallel.py  -i seis/raw/C200909020755A.observed.h5

