#!/bin/bash
#SBATCH --job-name=weights   
#SBATCH --exclusive             
#SBATCH --export=ALL            
#SBATCH --nodes=1             
#SBATCH --error=weights.err            
#SBATCH --output=weights.out            
#SBATCH --ntasks-per-node=36
#SBATCH --mem=190000            
#SBATCH --time=08:00:00         

#export cwd=`pwd`                                                                
#export PYTHONPATH="$cwd/user_functions":$PYTHONPATH   
e=C201108231751A
p=T045-110s


#pypaw-window_weights \
# -p parfile/window_weights.rec.all.$e.param.yml \
# -f paths/window_weights.rec.all.$e.path.json \
# -v
pypaw-window_weights \
 -p parfile/window_weights.all.$e.param.yml \
 -f paths/window_weights.all.$e.path.json \
 -v
