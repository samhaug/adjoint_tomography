#!/bin/bash
#SBATCH --job-name=weights   
#SBATCH --exclusive             
#SBATCH --export=ALL            
#SBATCH --nodes=1             
#SBATCH --error=weights.err            
#SBATCH --output=weights.out            
#SBATCH --ntasks-per-node=1
#SBATCH --mem=150000            
#SBATCH --time=01:00:00         

STARTTIME=$(date +%s)
echo "start time is : $(date +"%T")"

export gen="python ../generate_path_files.py -p ../paths.yml -s ../settings.yml -e ../event_list"

events=`$gen list events`
periods=`$gen list period_bands`
export GEOS_DIR=/ccs/proj/geo111/orsvuran/lib/rhea_geos/lib
export LD_LIBRARY_PATH=${GEOS_DIR}:${LD_LIBRARY_PATH}

i=0
for e in $events
do
    pypaw-window_weights \
     -p parfile/window_weights.all.$e.param.yml \
     -f paths/window_weights.all.$e.path.json \
     -v &
	  wait
    #pypaw-window_weights \
    # -p parfile/window_weights.rec.all.$e.param.yml \
    # -f paths/window_weights.rec.all.$e.path.json \
    # -v &
	  #wait
    #pypaw-window_weights \
  	#-p parfile/window_weights.rec.paired.$e.param.yml \
  	#-f paths/window_weights.rec.paired.$e.path.json \
  	#-v
    #pypaw-window_weights \
  	#-p parfile/window_weights.paired.$e.param.yml \
  	#-f paths/window_weights.paired.$e.path.json \
  	#-v
    #i=$(($i + 2))
    #if [ $i -ge $n_parallel_jobs ]; then
	#i=0
  #  fi
#    for p in $periods
#    do
#    	pypaw-window_weights \
#    	    -p parfile/window_weights.all.$e.$p.param.yml \
#    	    -f paths/window_weights.all.$e.$p.path.json \
#    	    -v
#    	pypaw-window_weights \
#    	    -p parfile/window_weights.paired.$e.$p.param.yml \
#    	    -f paths/window_weights.paired.$e.$p.path.json \
#    	    -v
#    done
done
wait

ENDTIME=$(date +%s)
Ttaken=$(($ENDTIME - $STARTTIME))
echo
echo "finish time is : $(date +"%T")"
echo "RUNTIME is :  $(($Ttaken / 3600)) hours ::  $(($(($Ttaken%3600))/60)) minutes  :: $(($Ttaken % 60)) seconds."

