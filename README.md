#Adjoint workflow

#Before you begin

Make sure to have the event\_list file updated.
Make specfem station file with generate\_specfem\_stations.py and add to
stations/ directory

Make sure you are in the python 2 environment:
source activate py2

Make sure settings.yml is correct

python generate\_path\_files.py generate folders

(Modules loaded for Wendian)
Currently Loaded Modules:
  1) compilers/intel/2019   2) mpi/impi/2019-intel   3) StdEnv   4) mpi/openmpi/3.1.3-intel
 
(Make sure you have a Python 2.7 environment)
source activate py2

Then do these steps in order:

#proc
python generate\_path\_files.py generate proc

#windowsel
python generate\_path\_files.py generate windows

#measure
python generate\_path\_files.py generate measure

#stations
python generate\_path\_files.py generate stations

#filter
python generate\_path\_files.py generate filter

#adjoint
python generate\_path\_files.py generate adjoint

Note that "paired", "single", and "dd" refer to the double difference

#weights
python generate\_path\_files.oy generate weight\_params

#sum\_adjoint
python generate\_path\_files.oy generate sum\_all

python add\_missing\_stations.py ADJOINT\_FILE.h5 ../station\_files/STATIONS\_FILE

