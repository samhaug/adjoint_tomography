#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

from generate_path_files import load_events
from generate_path_files import load_path_config
from generate_path_files import load_json
from pytomo3d.station.utils import write_stations_file

events = load_events("event_list")
folders, files = load_path_config("paths.yml")

for event in events:
    stations_data = load_json(files["stations_file"].format(eventname=event))
    sta_dict = {}
    for sta, data in stations_data.iteritems():
        if sta.endswith("Z"):
            net, sta, loc, comp = sta.split(".")
            sta_dict[".".join([net, sta])] = [data["latitude"],
                                              data["longitude"],
                                              data["elevation"],
                                              data["depth"]]
    write_stations_file(sta_dict, "STATIONS_"+event)
