#!/bin/bash
#SBATCH --job-name=proc_synt     
#SBATCH --qos=full
#SBATCH --exclusive             
#SBATCH --export=ALL            
#SBATCH --nodes=30             
#SBATCH --error=proc_synt.err            
#SBATCH --output=proc_synt.out            
#SBATCH --mem=180000            
#SBATCH --time=08:00:00         


# SAM_H this is the latest script to use as of 9/22/2019
source ~/.bashrc
module restore py2
source activate py2

STARTTIME=$(date +%s)
echo "start time is : $(date +"%T")"

export gen="python ../generate_path_files.py -p ../paths.yml -s ../settings.yml -e ../event_list"

events=`$gen list events`
periods=`$gen list period_bands`

nodes=(`scontrol show hostnames $SLURM_JOB_NODELIST`)
i=0
j=0
n_parallel_jobs=${#nodes[@]}
echo n_parallel_jobs: $n_parallel_jobs
echo 

for e in $events
do
    for p in $periods
    do
      echo "node: ${nodes[$i]}"
      srun -n36 -N1 -w ${nodes[$i]} \
            pypaw-process_asdf -p ./parfile/proc_synt.${p}.param.yml -f ./paths/proc_synt.${e}.${p}.path.json -v &
      i=$(($i + 1))
      if [ $i -ge $n_parallel_jobs ]; then
          i=0
          wait
          j=$(($j + 1))
          echo "done: $(($i*$j))"
      fi
    done
done
wait

ENDTIME=$(date +%s)
Ttaken=$(($ENDTIME - $STARTTIME))
echo
echo "finish time is : $(date +"%T")"
echo "RUNTIME is :  $(($Ttaken / 3600)) hours ::  $(($(($Ttaken%3600))/60)) minutes  :: $(($Ttaken % 60)) seconds."

