#!/bin/bash
#SBATCH --job-name=process      
#SBATCH --exclusive             
#SBATCH --export=ALL            
#SBATCH --nodes=1             
##SBATCH --error=process.err            
##SBATCH --output=process.out            
#SBATCH --ntasks-per-node=36
#SBATCH --mem=180000            
#SBATCH --time=08:00:00         


