#!/bin/bash
#SBATCH --job-name=process      
#SBATCH --qos=full
#SBATCH --exclusive             
#SBATCH --export=ALL            
#SBATCH --nodes=1             
#SBATCH --error=process.err            
#SBATCH --output=process.out            
#SBATCH --ntasks-per-node=36
#SBATCH --mem=187740           
#SBATCH --time=01:00:00         

period=$1
evt=$2
mpiexec -np 36 pypaw-process_asdf -p ./parfile/proc_obsd.${period}.param.yml -f ./paths/proc_obsd.${evt}.${period}.path.json -v
mpiexec -np 36 pypaw-process_asdf -p ./parfile/proc_synt.${period}.param.yml -f ./paths/proc_synt.${evt}.${period}.path.json -v
