#!/bin/bash
#SBATCH --job-name=process      
#SBATCH --exclusive             
#SBATCH --export=ALL            
#SBATCH --nodes=3             
#SBATCH --error=process.err            
#SBATCH --output=process.out            
#SBATCH --ntasks-per-node=36
#SBATCH --mem=187740           
#SBATCH --time=08:00:00         

STARTTIME=$(date +%s)
echo "start time is : $(date +"%T")"

export gen="python ../generate_path_files.py -p ../paths.yml -s ../settings.yml -e ../event_list"
events=`$gen list events`
periods=`$gen list period_bands`

for e in $events
do
    for p in $periods
    do
      echo "observed $e $p"
      mpiexec -np 108 pypaw-process_asdf -p ./parfile/proc_obsd.${p}.param.yml -f ./paths/proc_obsd.${e}.${p}.path.json -v 
      echo "synthetic $e $p"
      mpiexec -np 108 pypaw-process_asdf -p ./parfile/proc_synt.${p}.param.yml -f ./paths/proc_synt.${e}.${p}.path.json -v 
    done
done
wait

ENDTIME=$(date +%s)
Ttaken=$(($ENDTIME - $STARTTIME))
echo
echo "finish time is : $(date +"%T")"
echo "RUNTIME is :  $(($Ttaken / 3600)) hours ::  $(($(($Ttaken%3600))/60)) minutes  :: $(($Ttaken % 60)) seconds."
echo
echo "******************well done*******************************"


