#echo "start time is : $(date +"%T")"

export gen="python ../generate_path_files.py -p ../paths.yml -s ../settings.yml -e ../event_list"
events=`$gen list events`
periods=`$gen list period_bands`
i=0
j=0

for e in $events
do
    for p in $periods
    do
        if  [ $(($i%10)) == 0 ]; then
           j=$((j+1))
           #sbatch ./submit_process_num_${j}.sh
           echo "new job"
           cp submit_process_template.sh submit_process_num_${j}.sh
        fi
        echo "mpiexec -np 36 pypaw-process_asdf -p ./parfile/proc_obsd.${p}.param.yml -f ./paths/proc_obsd.${e}.${p}.path.json -v" >> submit_process_num_${j}.sh
        echo "mpiexec -np 36 pypaw-process_asdf -p ./parfile/proc_synt.${p}.param.yml -f ./paths/proc_synt.${e}.${p}.path.json -v" >> submit_process_num_${j}.sh
        i=$((i+1))
    done
done
wait

ENDTIME=$(date +%s)
Ttaken=$(($ENDTIME - $STARTTIME))
echo
echo "finish time is : $(date +"%T")"
echo "RUNTIME is :  $(($Ttaken / 3600)) hours ::  $(($(($Ttaken%3600))/60)) minutes  :: $(($Ttaken % 60)) seconds."
echo
echo "******************well done*******************************"


