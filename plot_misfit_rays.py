#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import matplotlib
matplotlib.use("agg")           # NOQA

import pyasdf
import argparse
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
import matplotlib.colors as colors
import matplotlib.cm as cmx
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from tqdm import tqdm


def plot_rays(ds, component):
    origin = ds.events[0].preferred_origin()
    origin_loc = (origin.longitude, origin.latitude)
    segments = []
    misfits = []
    for adj in tqdm(ds.auxiliary_data.AdjointSources):
        if adj.path.endswith(component):
            misfit = adj.parameters["misfit"]
            if misfit > 0:
                misfits.append(misfit)
                segments.append((origin_loc,
                                 (adj.parameters["longitude"],
                                  adj.parameters["latitude"])))

    proj = ccrs.Robinson()
    fig, ax = plt.subplots(figsize=(10, 5),
                           subplot_kw={"projection": proj})
    ax.coastlines()
    ax.add_feature(cfeature.LAND)
    ax.set_global()

    norm = colors.LogNorm(vmin=0.0001, vmax=200)
    scalarMap = cmx.ScalarMappable(norm=norm, cmap="rainbow")
    scalarMap.set_array(misfits)
    linecolors = [scalarMap.to_rgba(m) for m in misfits]

    lc = LineCollection(segments, zorder=5, colors=linecolors,
                        transform=ccrs.Geodetic())
    ax.add_collection(lc)
    cbar = plt.colorbar(scalarMap, ax=ax)
    cbar.set_label("Misfit")
    plt.tight_layout()
    return fig, ax


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Plot misfits as rays")
    parser.add_argument("adjoint_file", help="adjoint file (asdf)")
    parser.add_argument("component", choices="ZENRT",
                        help="component")
    parser.add_argument("output_file", help="output_file")
    args = parser.parse_args()
    ds = pyasdf.ASDFDataSet(args.adjoint_file)
    fig, ax = plot_rays(ds, args.component)
    fig.savefig(args.output_file)
