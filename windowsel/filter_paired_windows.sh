#!/bin/bash
#PBS -A GEO111
#PBS -N filter_paired_windows
#PBS -j oe
#PBS -l walltime=6:00:00,nodes=1

if [ -z $PBS_O_WORKDIR ]; then
    PBS_O_WORKDIR=`pwd`
else
    #module load python/2.7.9
    #source /ccs/proj/geo111/orsvuran/venvs/rhea_py2/bin/activate
fi

STARTTIME=$(date +%s)
echo "start time is : $(date +"%T")"
cd $PBS_O_WORKDIR

export gen="python ../generate_path_files.py -p ../paths.yml -s ../settings.yml -e ../event_list"

events=`$gen list events`
periods=`$gen list period_bands`

i=0
n_parallel_jobs=11
for e in $events
do
    for p in $periods
    do
	    pypaw-dd_filter_paired_windows \
            `$gen filename windows_filter_file $e $p`\
            `$gen filename dd_pairs_filter $e $p`
        i=$(($i + 1))
        if [ $i -ge $n_parallel_jobs ]; then
            i=0
            wait
        fi
    done
done
wait

ENDTIME=$(date +%s)
Ttaken=$(($ENDTIME - $STARTTIME))
echo
echo "finish time is : $(date +"%T")"
echo "RUNTIME is :  $(($Ttaken / 3600)) hours ::  $(($(($Ttaken%3600))/60)) minutes  :: $(($Ttaken % 60)) seconds."
