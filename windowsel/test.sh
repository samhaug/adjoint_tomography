data_dir=/gpfs/alpine/geo111/proj-shared/orsvuran/seis_data/proc

#for f in $(ls $data_dir/C*.obsd.{T090-250s,T045-110s}.h5); do
for f in $(ls $data_dir/C*.obsd.T090-250s.h5); do
   if [ $(echo $f | awk -F'.' '{print NF}') != 4 ]; then
       continue
   fi
   echo $f

done
