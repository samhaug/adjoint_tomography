#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import matplotlib.patches as patches
from geographiclib.geodesic import Geodesic
import numpy as np
import cartopy.crs as ccrs
import struct
import pyasdf
import argparse

from tqdm import tqdm

INSIDE = 0
LEFT = 1
RIGHT = 2
BOTTOM = 4
TOP = 8


def get_code(x, y, region):
    code = INSIDE
    if x < region.left:
        code |= LEFT
    elif x > region.right:
        code |= RIGHT

    if y > region.top:
        code |= TOP
    elif y < region.bottom:
        code |= BOTTOM
    return code


def get_src_and_rec_latlons(filename, tag):
    ds = pyasdf.ASDFDataSet(filename, mpi=False, mode="r")

    event = ds.events[0]
    origin = event.preferred_origin() or event.origins[0]
    src_lat = origin.latitude
    src_lon = origin.longitude
    stations = {}

    for waveform in ds.waveforms:
        try:
            sta_id = waveform[tag][0].id[:-1] + "Z"
        except pyasdf.WaveformNotInFileException:
            continue
        except KeyError:
            continue
        sta_name = ".".join(sta_id.split(".")[:2])
        if "coordinates" in waveform and "latitude" in waveform.coordinates:
            lat = waveform.coordinates["latitude"]
            lon = waveform.coordinates["longitude"]
        else:
            inv = waveform.StationXML
            coords = inv.get_coordinates(sta_id)
            lat = coords["latitude"]
            lon = coords["longitude"]
        stations[sta_name] = (lat, lon)

    return src_lat, src_lon, stations


class LatLon(object):
    """Documentation for LatLon

    """
    def __init__(self, lat, lon):
        super(LatLon, self).__init__()
        self.lat = lat
        self.lon = lon

    def __str__(self):
        return "LatLon ({}, {})".format(self.lat,
                                        self.lon)


class RayPathGlobal(object):
    def __init__(self, slat, slon, rlat, rlon, n_points=100, major_arc=False,
                 name=None):
        self.source = LatLon(slat, slon)
        self.receiver = LatLon(rlat, rlon)
        self.n_points = n_points
        self.major_arc = major_arc
        self.name = name
        self._create_raypath()

    def _create_raypath(self):
        self.p = Geodesic.WGS84.Inverse(self.source.lat, self.source.lon,
                                        self.receiver.lat, self.receiver.lon)

        self.dist = self.p['s12']
        self.azimuth = self.p['azi1']
        if self.major_arc:
            self.azimuth += 180
            self.dist = (360 - self.dist/111195.)*111195.
        self.l = Geodesic.WGS84.Line(self.p['lat1'], self.p['lon1'],
                                     self.azimuth)
        self.points = self.line(np.linspace(0, 1, self.n_points))

    def line(self, t):
        single_t = False
        if not isinstance(t, np.ndarray):
            t = np.array([t])
            single_t = True

        ps = np.zeros((len(t), 2))
        for i, v in enumerate(t):
            b = self.l.Position(v*self.dist)
            ps[i, :] = b["lat2"], b["lon2"]
        ps = np.array(ps)

        if single_t:
            ps = ps[0]
        return ps

    def plot(self, ax, **kwargs):
        ax.plot(self.points[:, 1], self.points[:, 0],
                transform=ccrs.Geodetic(), **kwargs)

    def in_region(self, region):
        for p in self.points:
            if get_code(p[1], p[0], region) == INSIDE:
                return True
        return False


class Region(object):
    def __init__(self, left, top, w, h):
        self.left = left
        self.top = top
        self.width = w
        self.height = h
        self.right = left+w
        self.bottom = top-h
        self.top_left = np.array([self.left, self.top])
        self.top_right = np.array([self.right, self.top])
        self.bottom_left = np.array([self.left, self.bottom])
        self.bottom_right = np.array([self.right, self.bottom])
        self.center = np.array([self.left+self.width/2,
                                self.top-self.height/2])
        self.region_type = None

    def in_360(self):
        return Region(self.left % 360, self.top,
                      self.width, self.height)

    def __eq__(self, other):
        if isinstance(other, Region):
            return other.left == self.left and other.top == self.top and \
                other.width == self.width and other.height == self.height
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        # pack the values into struct as short (2 bytes)
        p = struct.pack("hhhh", self.left, self.top, self.width, self.height)
        # unpack as a unsigned long long (8 bytes)
        region_hash = struct.unpack("Q", p)[0]
        return region_hash

    def __str__(self):
        return "Region ({}, {}, {}, {})".format(self.left,
                                                self.top,
                                                self.width,
                                                self.height)

    def __repr__(self):
        return self.__str__()

    def get_rect(self):
        return [self.left, self.top, self.width, self.height]

    def plot_rect(self, **kwargs):
        rect = patches.Rectangle(self.bottom_left,
                                 self.width,
                                 self.height,
                                 **kwargs)
        return rect


def create_regions(xmin, xmax, ymin, ymax, width, height):
    regions = []
    for left in range(xmin, xmax, width):
        for top in range(ymax, ymin, -height):
            regions.append(Region(left, top, width, height))
    return regions


def main(ds_filename, tag, output_filename, major_arc=False):
    src_lat, src_lon, stations = get_src_and_rec_latlons(ds_filename, tag)
    print("found file ", ds_filename)

    paths = []
    #for name, (lat, lon) in tqdm(stations.iteritems(), "Create Raypaths"):
    for name, (lat, lon) in stations.iteritems():
        paths.append(RayPathGlobal(src_lat, src_lon, lat, lon, major_arc=major_arc,
                                   name=name))

    continents_txt = r"""2 3 16 17
1 2 3 16 17
1 2 3 16 17
2 3 16 17
2 3 16 17
1 2 3 4 5 16 17
1 2 3 4 5 6 16 17
1 2 3 4 5 6 7 16 17
0 1 2 3 4 5 6 7 16 17
0 1 2 3 4 5 6 7 8 9 16 17
0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17
0 1 2 3 4 7 8 9 10 11 12 13 14 15 16 17
0 1 2 3 4 8 9 10 11 12 16 17
0 1 2 8 9 10 16 17
0 1 2 9 10 16 17
0 1 2 16 17
0 1 2 6 7 8 16 17
3 4 5 6 7 8 16 17
2 3 4 5 6 7 8 9 16 17
0 1 2 3 4 5 6 7 8 9 10 11 12 16 17
0 1 2 3 4 5 6 7 8 9 10 11 12 16 17
2 3 4 5 6 7 8 9 10 11 12 15 16 17
2 3 4 5 6 7 8 9 10 11 15 16 17
1 2 3 4 5 6 7 8 15 16 17
1 2 3 4 5 6 15 16 17
1 2 3 4 5 6 7 8 15 16 17
1 2 3 4 5 6 7 8 15 16 17
1 2 3 4 5 6 7 8 15 16 17
1 2 3 4 5 6 7 8 9 15 16 17
1 2 3 4 5 6 7 8 9 10 11 12 15 16 17
1 2 3 4 5 6 7 8 9 10 11 12 15 16 17
1 2 3 4 5 9 10 11 12 15 16 17
1 2 3 4 5 9 10 11 12 13 15 16 17
1 2 3 9 10 11 12 16 17
2 3 9 10 11 12 15 16 17
2 3 10 12 13 16 17"""

    regions = create_regions(-180, 180, -90, 90, 10, 10)
    continents = []
    for i, line in enumerate(continents_txt.splitlines()):
        for x in line.split():
            continents.append(int(x)+i*18)
    for i, region in enumerate(regions):
        if i in continents:
            region.region_type = "continent"
        else:
            region.region_type = "ocean"

    ocean_percentages = {}
    #for path in tqdm(paths, "Calc path percentage"):
    for path in paths:
        hits = []
        for region in regions:
            if path.in_region(region):
                hits.append(region.region_type)
        ocean_percentages[path.name] = hits.count("ocean")/len(hits)

    with open(output_filename, "w") as f:
        for name, per in ocean_percentages.iteritems():
            f.write("{} {:.3f}\n".format(name, per))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Find out ray path types")
    parser.add_argument("ds_filename", help="ds_filename")
    parser.add_argument("tag", help="tag")
    parser.add_argument("output_filename", help="output_filename")
    parser.add_argument("-m", "--major-arc", action="store_true", help="major-arc")
    args = parser.parse_args()
    main(args.ds_filename, args.tag, args.output_filename, args.major_arc)
