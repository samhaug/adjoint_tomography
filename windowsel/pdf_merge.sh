#!/bin/bash

cwd=`pwd`
for d in /gpfs/alpine/geo111/scratch/haugland/output_windows/C*/*; do 
    cd $d
    fname=$(pwd | awk -F'/' '{print $(NF-1)"."$NF".pdf"}')
    echo "Merging $fname"
    gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=$cwd/merged_pdfs/$fname *.pdf
    cd ../../..
done

