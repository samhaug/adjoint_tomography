#!/usr/bin/env python


import numpy as np

from obspy.geodetics import calc_vincenty_inverse

coverage_path = '/ccs/home/haugland/src/adjoint_tomography/windowsel/raypath_percentages'
passing_path = '/ccs/home/haugland/src/adjoint_tomography/windowsel/pass_values'

def get_dist_in_km(station, event, obsd, synt):
    """
    Returns distance in km
    """
    synt_stats = synt.stats
    print("love_90_250")
    print(station)
    print("synt_stats_"+".".join([synt_stats.network,synt_stats.station,synt_stats.location,synt_stats.channel[:-1]+"Z"]))
    obsd_stats = obsd.stats
    print("obsd_stats_"+".".join([obsd_stats.network,obsd_stats.station,obsd_stats.location,obsd_stats.channel[:-1]+"Z"]))
    try:
        station_coor = station.get_coordinates(".".join([synt_stats.network,
                                                         synt_stats.station,
                                                         synt_stats.location,
                                                         synt_stats.channel[:-1]+"Z"]))
    except:
        station_coor = station.get_coordinates(".".join([obsd_stats.network,
                                                         obsd_stats.station,
                                                         obsd_stats.location,
                                                         obsd_stats.channel[:-1]+"Z"]))

    evlat = event.origins[0].latitude
    evlon = event.origins[0].longitude

    dist = calc_vincenty_inverse(station_coor["latitude"],
                                 station_coor["longitude"],
                                 evlat,evlon)[0] / 1000.

    return dist


def get_time_array(obsd, event):
    stats = obsd.stats
    dt = stats.delta
    npts = stats.npts
    start = stats.starttime - event.origins[0].time
    return np.arange(start, start+npts*dt, dt)

def r1_percent_find(stats, event):
    try:
        fname = 'C'+event.event_descriptions[0].text+'.obsd.T090-250s.raycover.min.dat'
        f = open(coverage_path+'/'+fname,'r').readlines()
        label_list = [i.strip().split()[0] for i in f]
        percent_list = [i.strip().split()[1] for i in f]
        label = stats.network+'.'+stats.station
        idx = label_list.index(label)
        print("found r1 ",label)
        return float(percent_list[idx])
    except:
        label = stats.network+'.'+stats.station
        print("not found r1 :", label)
        return 'none'

def r2_percent_find(stats, event):
    try:
        fname = 'C'+event.event_descriptions[0].text+'.obsd.T090-250s.raycover.maj.dat'
        f = open(coverage_path+'/'+fname,'r').readlines()
        label_list = [i.strip().split()[0] for i in f]
        percent_list = [i.strip().split()[1] for i in f]
        label = stats.network+'.'+stats.station
        idx = label_list.index(label)
        print("found r2 ",label)
        return float(percent_list[idx])
    except:
        label = stats.network+'.'+stats.station
        print("not found r2 :", label)
        return 'none'

def passing_find(stats,event):
    fname = 'C'+event.event_descriptions[0].text
    try:
        f = open(passing_path+'/'+fname+'/T090-250s/new_values.txt','r').readlines()
        red_green_list = [ i.strip().split()[-1] for i in f ]
        label_list = [ i.strip().split()[0] for i in f ]
        if len(label_list) == 0:
            print("label_list empty: ",fname)

        label = stats.network+'.'+stats.station+'.'+stats.location+'.'+stats.channel
        print("passing_path: ",passing_path+'/'+fname+'/T090-250s/new_values.txt')
        idx = label_list.index(label)
        red_green = red_green_list[idx]
    except:
        return 'red'

    print("green :",label)
    return red_green


def generate_user_levels(config, station, event, obsd, synt):
    """
    Returns a list of water levels
    """

    stats = obsd.stats
    npts = stats.npts

    base_water_level = config.stalta_waterlevel
    base_cc = config.cc_acceptance_level
    base_tshift = config.tshift_acceptance_level
    base_dlna = config.dlna_acceptance_level
    base_s2n = config.s2n_limit

    stalta_waterlevel = np.ones(npts)*base_water_level*10
    cc = np.ones(npts)*base_cc
    tshift = np.ones(npts)*base_tshift
    dlna = np.ones(npts)*base_dlna
    s2n = np.ones(npts)*base_s2n

    dist = get_dist_in_km(station, event, obsd, synt)
    times = get_time_array(synt, event)

    red_green = passing_find(stats,event)
    if red_green == 'red':
        return stalta_waterlevel, tshift, dlna, cc, s2n

    elif red_green =='green':

        earth_circum=40075.
        half_win_r1 = 0.3
        half_win_r2 = 0.2
        ocean_vel = 4.4
        continent_vel = 4.4

        r1_percent = r1_percent_find(stats,event)
        r2_percent = r2_percent_find(stats,event)

        if r1_percent == 'none' or r2_percent == 'none':
            r1_percent = 0.5
            r2_percent = 0.5

        r1_vel = r1_percent*ocean_vel+(1-r1_percent)*continent_vel
        r2_vel = r2_percent*ocean_vel+(1-r2_percent)*continent_vel

        R1_t_min = int(dist/(r1_vel+half_win_r1))
        R1_t_max = int(dist/(r1_vel-half_win_r1))

        R2_t_min = int((earth_circum-dist)/(r2_vel+half_win_r2))
        R2_t_max = int((earth_circum-dist)/(r2_vel-half_win_r2))

        print("R1: ",str(R1_t_min),str(R1_t_max))
        print("R2: ",str(R2_t_min),str(R2_t_max))
        print("len: ",str(len(stalta_waterlevel)))

        stalta_waterlevel[R1_t_min:R1_t_max] = base_water_level
        stalta_waterlevel[R2_t_min:R2_t_max] = base_water_level*1.5

        label = stats.network+'.'+stats.station+'.'+stats.location+'.'+stats.channel
        print("adjusted_waterlevel ",'C'+event.event_descriptions[0].text,label)

        tshift[R2_t_min:R2_t_max] *= 1.5
        print('love_90 success')

        return stalta_waterlevel, tshift, dlna, cc, s2n



