#!/bin/bash
#SBATCH --job-name=windowsel     
#SBATCH --exclusive             
#SBATCH --export=ALL            
#SBATCH --nodes=3             
#SBATCH --error=windowsel.err            
#SBATCH --output=windowsel.out            
#SBATCH --ntasks-per-node=36
#SBATCH --mem=187740           
#SBATCH --time=08:00:00         

STARTTIME=$(date +%s)
echo "start time is : $(date +"%T")"

#export cwd=`pwd`
#export PYTHONPATH="$cwd/user_functions":$PYTHONPATH

export gen="python ../generate_path_files.py -p ../paths.yml -s ../settings.yml -e ../event_list"

events=`$gen list events`
periods=`$gen list period_bands`

for e in $events
do
    for p in $periods
    do
      echo $e $p
	    mpiexec -n 108 pypaw-window_selection_asdf \
	    -p ./parfile/window.${p}.param.yml \
	    -f ./paths/windows.${e}.${p}.path.json \
	    -v & 
      wait
    done
done
wait

ENDTIME=$(date +%s)
Ttaken=$(($ENDTIME - $STARTTIME))
echo
echo "finish time is : $(date +"%T")"
echo "RUNTIME is :  $(($Ttaken / 3600)) hours ::  $(($(($Ttaken%3600))/60)) minutes  :: $(($Ttaken % 60)) seconds."
echo "****************** JOB DONE ******************"
