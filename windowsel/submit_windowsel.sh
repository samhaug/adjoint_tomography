#!/bin/bash
#SBATCH --job-name=windowsel     
#SBATCH --exclusive             
#SBATCH --export=ALL            
#SBATCH --nodes=1             
#SBATCH --error=windowsel.err            
#SBATCH --output=windowsel.out            
#SBATCH --ntasks-per-node=36
#SBATCH --mem=190000            
#SBATCH --time=08:00:00         

export cwd=`pwd`                                                                
export PYTHONPATH="$cwd/user_functions":$PYTHONPATH   
e=C201108231751A
p=T045-110s

mpiexec -n 36 pypaw-window_selection_asdf -p ./parfile/window.$p.param.yml  -f ./paths/windows.$e.$p.path.json -v
