#!/bin/bash
#SBATCH --job-name=window
#SBATCH --account=GEO111                                                        
#SBATCH --exclusive                                                             
#SBATCH --export=ALL                                                            
#SBATCH --nodes=20                                                        
#SBATCH --time=12:00:00        
#SBATCH --output=raypath.out       
#SBATCH --error=raypath.err      

# SAM_H this is the latest script to use as of 9/22/2019

source ~/.bashrc
module restore pypaw
source activate python2

#export cwd=`pwd`
#export PYTHONPATH="$cwd/user_functions":$PYTHONPATH

cur=/ccs/home/haugland/src/adjoint_tomography/windowsel

STARTTIME=$(date +%s)
echo "start time is : $(date +"%T")"

nodes=(`scontrol show hostnames $SLURM_JOB_NODELIST`)
i=0
j=0
n_parallel_jobs=${#nodes[@]}
echo n_parallel_jobs: $n_parallel_jobs
echo

data_dir=/gpfs/alpine/geo111/proj-shared/orsvuran/seis_data/proc
make_rays=/gpfs/alpine/geo111/scratch/haugland/output_windows/make_rays.txt

#for f in $(ls $data_dir/C*.obsd.{T090-250s,T045-110s}.h5); do
while read f; do
   if [ $(echo $f | awk -F'.' '{print NF}') != 4 ]; then
       continue
   fi
   file=$(echo $f | awk -F'/' '{print $NF}')
   min_name=${file%.*}.raycover.min.dat
   maj_name=${file%.*}.raycover.maj.dat
   
   #if [ -f ${cur}/raypath_percentages/$maj_name ]; then
   #    echo "FILE EXISTS" ${cur}/raypath_percentages/$maj_name 
   #    continue
   #fi

   #if [ ! -f ${cur}/raypath_percentages/$maj_name ]; then
   echo "processing major: " ${cur}/raypath_percentages/$maj_name >> major_process.dat
   #srun -n1 -N1 -w ${nodes[$i]} \
   #   python raypath_types.py $f 'proc_obsd' ${cur}/raypath_percentages/$min_name &
   srun -n1 -N1 -w ${nodes[$i]} \
      python raypath_types.py $f 'proc_obsd' ${cur}/raypath_percentages/$maj_name -m &
   i=$(($i + 1))
   #if [ $i -ge $n_parallel_jobs ]; then                                      
   #    i=0                                                                   
   #    wait                                                                  
   #    j=0                                                        
   #    echo "done: $(($i*$j))"                                               
   #fi                                 
   #fi 
done < $make_rays

ENDTIME=$(date +%s)
Ttaken=$(($ENDTIME - $STARTTIME))
echo
echo "finish time is : $(date +"%T")"
echo "RUNTIME is :  $(($Ttaken / 3600)) hours ::  $(($(($Ttaken%3600))/60)) minutes  :: $(($Ttaken % 60)) seconds."

