import json
import glob

T45 = glob.glob('*/T045-110s')
T90 = glob.glob('*/T090-250s')

T_list_R1 = 0
T_list_R2 = 0

Z_list_R1 = 0
Z_list_R2 = 0

R_list_R1 = 0
R_list_R2 = 0

for i in T45:
    evt  = i.strip().split('/')
    f = open(i+'/windows.json')
    jw = json.load(f)
    for keys in jw:
        for skeys in jw[keys]:
            if len(jw[keys][skeys]) != 0:
                for idx,ii in enumerate(jw[keys][skeys]):
                    if idx == 2:
                        continue
                    mid_time  = (ii['relative_starttime']+ii['relative_endtime'])/2.
                    component = skeys.strip().split('.')[-1]
                    if 'BHR' in ii['channel_id']:
                        if float(mid_time) > 5000:
                            R_list_R2 += 1
                        else:
                            R_list_R1 += 1
                    elif 'BHZ' in ii['channel_id']:
                        if float(mid_time) > 5000:
                            Z_list_R2 += 1
                        else:
                            Z_list_R1 += 1
                    elif 'BHT' in ii['channel_id']:
                        if float(mid_time) > 5000:
                            T_list_R2 += 1
                        else:
                            T_list_R1 += 1

print("T45-110s statistics--------------\n")
print(" %s: %d"%("# of vertical R1",Z_list_R1))
print(" %s: %d\n"%("# of vertical R2",Z_list_R2))
print(" %s: %d"%("# of radial R1",R_list_R1))
print(" %s: %d\n"%("# of radial R2",R_list_R2))
print(" %s: %d"%("# of transverse R1",T_list_R1))
print(" %s: %d\n"%("# of transverse R2",T_list_R2))

T_list_R1 = 0
T_list_R2 = 0

Z_list_R1 = 0
Z_list_R2 = 0

R_list_R1 = 0
R_list_R2 = 0

zero_traces_R = 0
zero_traces_T = 0
zero_traces_Z = 0

R_traces = 0
Z_traces = 0
T_traces = 0

R_windows = 0
T_windows = 0
Z_windows = 0
skip_evt = open("T45_skip_events.txt",'w')

for i in T45:
    evt  = i.strip().split('/')[-2]
    f = open(i+'/windows.stats.json')
    j = json.load(f)

    try:
        r_trace = j['component']['BHR']['traces']
        t_trace = j['component']['BHT']['traces']
        z_trace = j['component']['BHZ']['traces']
    except:
        continue

    r_traces_windows = j['component']['BHR']['traces_with_windows']
    t_traces_windows = j['component']['BHT']['traces_with_windows']
    z_traces_windows = j['component']['BHZ']['traces_with_windows']

    r_windows = j['component']['BHR']['windows']
    t_windows = j['component']['BHT']['windows']
    z_windows = j['component']['BHZ']['windows']

    if r_traces_windows == 0:
        zero_traces_R += 1
    else:
        R_traces += r_trace
        R_windows += r_traces_windows

    if z_traces_windows == 0:
        zero_traces_Z += 1
    else:
        Z_traces += z_trace
        Z_windows += z_traces_windows

    if t_traces_windows == 0:
        zero_traces_T += 1
    else:
        T_traces += t_trace
        T_windows += t_traces_windows

    if t_traces_windows == 0 and z_traces_windows == 0 and t_traces_windows == 0:
        skip_evt.write(evt+'\n')

skip_evt.close()

print(" %s: %3.1f %%"%("% of Z with windows",100*(float(Z_windows)/Z_traces)))
print(" %s: %3.1f %%"%("% of T with windows",100*(float(T_windows)/T_traces)))
print(" %s: %3.1f %%\n"%("% of R with windows",100*(float(R_windows)/R_traces)))
#print(" %s: %d"%("Z skips",zero_traces_Z))
#print(" %s: %d"%("T skips",zero_traces_T))
#print(" %s: %d\n"%("R skips",zero_traces_R))

for i in T90:
    evt  = i.strip().split('/')
    f = open(i+'/windows.json')
    jw = json.load(f)
    for keys in jw:
        for skeys in jw[keys]:
            if len(jw[keys][skeys]) != 0:
                for idx,ii in enumerate(jw[keys][skeys]):
                    if idx == 2:
                        continue
                    mid_time  = (ii['relative_starttime']+ii['relative_endtime'])/2.
                    component = skeys.strip().split('.')[-1]
                    if 'BHR' in ii['channel_id']:
                        if float(mid_time) > 5000:
                            R_list_R2 += 1
                        else:
                            R_list_R1 += 1
                    elif 'BHZ' in ii['channel_id']:
                        if float(mid_time) > 5000:
                            Z_list_R2 += 1
                        else:
                            Z_list_R1 += 1
                    elif 'BHT' in ii['channel_id']:
                        if float(mid_time) > 5000:
                            T_list_R2 += 1
                        else:
                            T_list_R1 += 1

print("T90-250s statistics--------------\n")
print(" %s: %d"%("# of vertical R1",Z_list_R1))
print(" %s: %d\n"%("# of vertical R2",Z_list_R2))
print(" %s: %d"%("# of radial R1",R_list_R1))
print(" %s: %d\n"%("# of radial R2",R_list_R2))
print(" %s: %d"%("# of transverse R1",T_list_R1))
print(" %s: %d\n"%("# of transverse R2",T_list_R2))



zero_traces_R = 0
zero_traces_T = 0
zero_traces_Z = 0

R_traces = 0
Z_traces = 0
T_traces = 0

R_windows = 0
T_windows = 0
Z_windows = 0
skip_evt = open("T90_skip_events.txt",'w')

for i in T90:
    evt  = i.strip().split('/')[-2]
    f = open(i+'/windows.stats.json')
    j = json.load(f)

    try:
        r_trace = j['component']['BHR']['traces']
        t_trace = j['component']['BHT']['traces']
        z_trace = j['component']['BHZ']['traces']
    except:
        continue

    r_traces_windows = j['component']['BHR']['traces_with_windows']
    t_traces_windows = j['component']['BHT']['traces_with_windows']
    z_traces_windows = j['component']['BHZ']['traces_with_windows']

    r_windows = j['component']['BHR']['windows']
    t_windows = j['component']['BHT']['windows']
    z_windows = j['component']['BHZ']['windows']

    if r_traces_windows == 0:
        zero_traces_R += 1
    else:
        R_traces += r_trace
        R_windows += r_traces_windows

    if z_traces_windows == 0:
        zero_traces_Z += 1
    else:
        Z_traces += z_trace
        Z_windows += z_traces_windows

    if t_traces_windows == 0:
        zero_traces_T += 1
    else:
        T_traces += t_trace
        T_windows += t_traces_windows

    if t_traces_windows == 0 and z_traces_windows == 0 and t_traces_windows == 0:
        skip_evt.write(evt+'\n')

skip_evt.close()

print(" %s: %3.1f %%"%("% of Z with windows",100*(float(Z_windows)/Z_traces)))
print(" %s: %3.1f %%"%("% of T with windows",100*(float(T_windows)/T_traces)))
print(" %s: %3.1f %%\n"%("% of R with windows",100*(float(R_windows)/R_traces)))
#print(" %s: %d"%("Z skips",zero_traces_Z))
#print(" %s: %d"%("T skips",zero_traces_T))
#print(" %s: %d\n"%("R skips",zero_traces_R))




