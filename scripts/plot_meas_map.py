#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm
from pytomo3d.doubledifference.utils import get_stanames_of_pair

import argparse
import json

import matplotlib.colors as colors
import matplotlib.cm as cmx
from geographiclib.geodesic import Geodesic
from obspy.geodetics  import locations2degrees
import cartopy.crs as ccrs
import cartopy

import pyasdf


def read_json(filename):
    with open(filename) as f:
        return json.load(f)

def get_paired_phase(pairs, comp, w_i, w_j, order):
    if order.endswith("_2"):
        w_i, w_j = w_j, w_i
    for pair in pairs[comp]:
        if w_i == pair["window_id_i"] and w_j == pair["window_id_j"]:
            return pair["paired_phase"]
    raise Exception("Pair not found for:", w_i, w_j)


def calc_misfit_sums(measurements, pairs, phase_type):
    misfits = {}
    for comp, comp_measurements in measurements.iteritems():
        for window in comp_measurements:
            window_meas = comp_measurements[window]
            paired_phase = get_paired_phase(pairs, comp,
                                            window,
                                            window_meas[0]["paired_with"],
                                            window_meas[0]["type"])
            if paired_phase == phase_type:
                comp_name = window.split(":")[0]
                misfits[comp_name] = sum([meas["misfit"]
                                          for meas in window_meas])
    return misfits


def get_latlon(station, stations):
    s = station.replace("BHR", "BHZ").replace("BHT", "BHZ")
    return (stations[s]["latitude"],
            stations[s]["longitude"])


def get_comp_misfits(misfits, stations, comp):
    lats = []
    lons = []
    comp_misfits = []
    for name, misfit in misfits.iteritems():
        if name.endswith(comp):
            lat, lon = get_latlon(name, stations)
            lats.append(lat)
            lons.append(lon)
            comp_misfits.append(misfit)
    return lats, lons, comp_misfits


def get_line(src_lat, src_lon, lat, lon, arc="R1", num=15):
    p = Geodesic.WGS84.Inverse(src_lat, src_lon, lat, lon)

    if arc == "R1":
        l = Geodesic.WGS84.Line(p['lat1'], p['lon1'], p['azi1'])
        dist = p['s12']
    elif arc == "R2":
        l = Geodesic.WGS84.Line(p['lat1'], p['lon1'], 180+p['azi1'])
        dist = 360*111*1000 - p['s12']
    else:
        raise ValueError("arc:", arc)

    line_lats = []
    line_lons = []
    for i in range(num+1):
        b = l.Position(i*dist/num)
        line_lats.append(b["lat2"])
        line_lons.append(b["lon2"])
    return line_lats, line_lons

def map_misfits(misfits, stations, event, components_to_plot, phase_type):
    figs = []
    event_lat = event.preferred_origin().latitude
    event_lon = event.preferred_origin().longitude
    for comp in components_to_plot:
        lats, lons, comp_misfits = get_comp_misfits(misfits, stations, comp)
        fig = plt.figure(figsize=(12, 8))
        ax = plt.axes(projection=ccrs.Mollweide(
            central_longitude=event_lon))
        cNorm = colors.LogNorm(vmin=0.001, vmax=100)
        scalarMap = cmx.ScalarMappable(norm=cNorm, cmap="jet")
        scalarMap.set_array(comp_misfits)
        ax.add_feature(cartopy.feature.OCEAN, zorder=0,
                       facecolor=np.array((74, 128, 245))/255)
        ax.add_feature(cartopy.feature.LAND, zorder=0,
                       facecolor=np.array((241, 141, 0))/255,
                       edgecolor='black')
        ax.set_global()
        ax.gridlines()

        for lat, lon, m in zip(lats, lons, comp_misfits):
            llats, llons = get_line(event_lat, event_lon,
                                    lat, lon,
                                    phase_type,
                                    num=100)
            ax.plot(llons, llats, color=scalarMap.to_rgba(m),
                    transform=ccrs.Geodetic())
        ax.scatter(event_lon, event_lat, 100, c="red", marker="*",
                   zorder=200,
                   transform=ccrs.Geodetic())
        plt.colorbar(scalarMap)
        figs.append(fig)
    return figs

def plot_meas_map(measurements, pairs, stations, event,
                  phase_type="R1", components_to_plot="ZRT",
                  filename_prefix=""):

    misfits = calc_misfit_sums(measurements, pairs, phase_type)
    figs = map_misfits(misfits, stations, event, components_to_plot,
                       phase_type)
    plt.tight_layout()
    if filename_prefix == "":
        plt.show()
    else:
        for fig, c in zip(figs, components_to_plot):
            filename = filename_prefix+"_{}.png".format(c)
            print("writing figure to:", filename)
            fig.savefig(filename)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Plot measurement misfit on a map')
    parser.add_argument('meas_file', help="measurement file")
    parser.add_argument('pairs_file', help="pairs file")
    parser.add_argument('stations_file', help="stations file")
    parser.add_argument('event_ds',
                        help="ASDF file that contains event info")
    parser.add_argument('-t', '--phase-type', choices=("R1", "R2"), default="R1",
                        help="type of phase to map")
    parser.add_argument('-c', '--components-to-plot', default="ZRT",
                        help="components to plot")
    parser.add_argument('-f', '--filename-prefix', default="",
                        help="figure filename prefix")

    args = parser.parse_args()

    measurements = read_json(args.meas_file)
    pairs = read_json(args.pairs_file)
    stations = read_json(args.stations_file)
    with pyasdf.ASDFDataSet(args.event_ds) as ds:
        event = ds.events[0]
    plot_meas_map(measurements, pairs, stations, event,
                  args.phase_type, args.components_to_plot,
                  args.filename_prefix)

