#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import matplotlib.pyplot as plt
import numpy as np
import glob
from tqdm import tqdm
import pandas as pd
# import seaborn as sns
import json


def find_meas(paired_measurements, paired_with):
    for meas in paired_measurements:
        if paired_with == meas["paired_with"]:
            return meas
    raise ValueError()


def load_json(filename):
    with open(filename) as f:
        return json.load(f)


pair_files = glob.glob("dd/output/dd_pairs.*.*.json")
data = []
periods = set()
events = set()

for pair_file in tqdm(pair_files):
    e, p = pair_file.split(".")[2:4]
    events.add(e)
    periods.add(p)
    meas_file = "measure/dd_output/dd_measure.{e}.{p}.json".format(e=e,
                                                                   p=p)
    pairs = load_json(pair_file)
    measurements = load_json(meas_file)
    for comp, comp_pairs in pairs.iteritems():
        for pair in comp_pairs:
            i = pair["window_id_i"]
            j = pair["window_id_j"]
            meas = find_meas(measurements[comp][i], j)
            data.append({
                "event": e,
                "period": p,
                "cc": pair["cc_similarity"],
                "dist": pair["distance"],
                "phase": pair["paired_phase"],
                "ddt": meas["ddt"],
                "misfit": meas["misfit"],
            })

data = pd.DataFrame(data)
print(data.phase.value_counts())

for p in periods:
    fig, axes = plt.subplots(ncols=2, sharey=True, figsize=(10, 5))
    fig.suptitle("Period: {}".format(p))
    p_data = data[data.period == p]
    minp = int(p.split("-")[0][1:])
    # axes[0].plot(p_data.dist, np.abs(p_data.ddt), "bo")
    axes[0].plot(p_data[p_data.phase == "R1"].dist,
                 np.abs(p_data[p_data.phase == "R1"].ddt), "bo")
    axes[0].plot(p_data[p_data.phase == "R2"].dist,
                 np.abs(p_data[p_data.phase == "R2"].ddt), "ro")
    axes[0].plot([p_data.dist.min(), p_data.dist.max()],
                 [minp/2, minp/2], "r--")
    axes[0].set_yscale("log")
    axes[0].set_xlabel("Distance (km)")
    axes[0].set_xlim([p_data.dist.min(), p_data.dist.max()])
    axes[0].grid()
    # axes[0].set_ylim([1e-6, 1e3])
    axes[0].set_ylabel("DD dt")
    axes[1].plot(p_data[p_data.phase == "R1"].cc,
                 np.abs(p_data[p_data.phase == "R1"].ddt), "bo")
    axes[1].plot(p_data[p_data.phase == "R2"].cc,
                 np.abs(p_data[p_data.phase == "R2"].ddt), "ro")
    axes[1].plot([p_data.cc.min(), p_data.cc.max()],
                 [minp/2, minp/2], "r--")
    axes[1].invert_xaxis()
    axes[1].set_xlim([p_data.cc.min(), p_data.cc.max()])
    axes[1].set_xlabel("CC Similarity")
    axes[1].grid()
plt.tight_layout()
plt.show()
