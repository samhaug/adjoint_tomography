#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy.feature as cfeature

from generate_path_files import load_events
from generate_path_files import load_path_config
from generate_path_files import load_json
from generate_path_files import load_yaml
from generate_path_files import f
import generate_path_files

import argparse

from collections import defaultdict
import pickle
import os

from tqdm import tqdm


def get_sta_loc(st, stations):
    for name in stations:
        if name.startswith(st):
            return get_latlon(name, stations)


def get_latlon(st, stations):
    return (stations[st]["latitude"],
            stations[st]["longitude"])


def get_plot_data(pairs, stations):
    dots = {}
    lines = set()
    n_pairs = defaultdict(int)
    for pair in pairs:
        st_i = pair["window_id_i"].split(":")[0]
        st_j = pair["window_id_j"].split(":")[0]
        lat_i, lon_i = get_latlon(st_i, stations)
        lat_j, lon_j = get_latlon(st_j, stations)
        dots[".".join(st_i.split(".")[:2])] = (lat_i, lon_i)
        dots[".".join(st_j.split(".")[:2])] = (lat_j, lon_j)
        lines.add((lat_i, lon_i, lat_j, lon_j))
        n_pairs[".".join(st_i.split(".")[:2])] += 1
        n_pairs[".".join(st_j.split(".")[:2])] += 1
    return dots, lines, n_pairs


def plot_stations(stations, locations, use_number=True):
    proj = ccrs.Robinson()
    fig, ax = plt.subplots(figsize=(10, 5),
                           subplot_kw={"projection": proj})
    ax.coastlines()
    ax.add_feature(cfeature.LAND)
    ax.set_global()
    counts = []
    lats = []
    lons = []
    for sta, count in tqdm(stations.iteritems(), "Create Points",
                           total=len(stations)):
        counts.append(count)
        lat, lon = locations[sta]
        lats.append(lat)
        lons.append(lon)

    if use_number:
        sc = ax.scatter(lons, lats, c=counts,
                        marker="v", s=50, cmap="viridis",
                        zorder=1000, transform=ccrs.Geodetic())
        cbar = plt.colorbar(sc, fraction=0.024, pad=0.02, ax=ax)
        cbar.set_label("Number of earthquakes")
    else:
        sc = ax.scatter(lons, lats, 50, marker="v",
                        edgecolor="black",
                        transform=ccrs.Geodetic())
    return fig, ax


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Plot receivers")
    parser.add_argument("-n", "--show-number",
                        action="store_true",
                        help="show how many times receiver is used.")
    args = parser.parse_args()

    events = load_events("event_list")
    folders, files = load_path_config("paths.yml")
    settings = load_yaml("settings.yml")
    periods = settings["period_bands"]

    generate_path_files.folders = folders
    generate_path_files.files = files

    used_stations = defaultdict(int)
    locations = {}
    print("Get window data")
    if os.path.isfile("receiver_data.pkl"):
        with open("receiver_data.pkl", "rb") as pf:
            data = pickle.load(pf)
        used_stations = data["used_stations"]
        locations = data["locations"]
    else:
        pbar = tqdm(total=len(events)*len(periods),
                    desc="Reading windows data")
        for event in events:
            for period in periods:
                stations = load_json(f("stations_file", event, period))
                windows = load_json(f("windows_filter_file", event, period))
                for sta in windows:
                    used_stations[sta] += 1
                    locations[sta] = get_sta_loc(sta, stations)
                pbar.update()
        with open("receiver_data.pkl", "wb") as pf:
            data = {}
            data["used_stations"] = dict(used_stations)
            data["locations"] = locations
            pickle.dump(data, pf)

    fig, ax = plot_stations(used_stations, locations, args.show_number)
    fig.savefig("receiver_map.pdf")
