#!/bin/bash
#SBATCH --job-name=process
#SBATCH --exclusive
#SBATCH --export=ALL
#SBATCH --nodes=10
#SBATCH --ntasks-per-node=36
#SBATCH --mem-per-cpu=1024
#SBATCH --time=10:00:00


if [ -z $PBS_O_WORKDIR]; then
    PBS_O_WORKDIR=`pwd`
else
    module load python/2.7.9
    source /ccs/proj/geo111/orsvuran/venvs/rhea_py2/bin/activate
fi

if [ -z $PBS_NODEFILE]; then
    PBS_NODEFILE=`mktemp`
    echo $HOST > $PBS_NODEFILE
fi


STARTTIME=$(date +%s)
echo "start time is : $(date +"%T")"


echo $PBS_O_WORKDIR
cd $PBS_O_WORKDIR

export gen="python ../generate_path_files.py -p ../paths.yml -s ../settings.yml -e ../event_list"
events=`$gen list events`
periods=`$gen list period_bands`



nodes=(`scontrol show hostnames $SLURM_JOB_NODELIST`)
#nodes=($(sort $PBS_NODEFILE | uniq | sed 's/:.*//'))
i=0
n_parallel_jobs=${#nodes[@]}
echo n_parallel_jobs: $n_parallel_jobs

for e in $events
do
    for p in $periods
    do
		echo "node: ${nodes[$i]}"
		echo "event: $e"
		# -p partition -w${nodes[$i]} 
		# srun -n36 --mem-per-cpu=512 sh proc_synt.sh $e $p &
		mpirun -np 36 --map-by node -host ${nodes[$i]} sh proc_one.sh $e $p &
		i=$(($i + 1))
		if [ $i -ge $n_parallel_jobs ]; then
			i=0
			wait
		fi
    done
done
wait

ENDTIME=$(date +%s)
Ttaken=$(($ENDTIME - $STARTTIME))
echo
echo "finish time is : $(date +"%T")"
echo "RUNTIME is :  $(($Ttaken / 3600)) hours ::  $(($(($Ttaken%3600))/60)) minutes  :: $(($Ttaken % 60)) seconds."

echo
echo "******************well done*******************************"


