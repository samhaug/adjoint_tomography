#!/bin/bash
#PBS -A GEO111
#PBS -N dd_pair
#PBS -j oe
#PBS -l walltime=12:00:00,nodes=1

if [ -z $PBS_O_WORKDIR ]; then
    PBS_O_WORKDIR=`pwd`
else
    module load python/2.7.9
    source /ccs/proj/geo111/orsvuran/venvs/rhea_py2/bin/activate
fi

if [ -z $PBS_NODEFILE ];  then
    PBS_NODEFILE=`mktemp`
    echo $HOST > $PBS_NODEFILE
fi

STARTTIME=$(date +%s)
echo "start time is : $(date +"%T")"

echo $PBS_O_WORKDIR
cd $PBS_O_WORKDIR


export gen="python ../generate_path_files.py -p ../paths.yml -s ../settings.yml -e ../event_list"

events=`$gen list events`
periods=`$gen list period_bands`

i=0
n_parallel_jobs=11
for e in $events
do
    for p in $periods
    do
	pypaw-dd_find_pairs \
	    -p ./parfile/dd.yml \
	    -f ./paths/dd.$e.$p.path.json \
	    -v &
	i=$(($i + 1))
	if [ $i -ge $n_parallel_jobs ]; then
	    i=0
	    wait
	    echo "----"
	    echo "$e.$p"
	    echo "Number of R1:"
	    grep '"paired_phase": "R1"' output/dd_pairs.$e.$p.json | wc -l
	    echo "Number of R2:"
	    grep '"paired_phase": "R2"' output/dd_pairs.$e.$p.json | wc -l
	    echo "Number of G1:"
	    grep '"paired_phase": "R1"' output/dd_pairs.$e.$p.json | wc -l
	    echo "Number of G2:"
	    grep '"paired_phase": "R2"' output/dd_pairs.$e.$p.json | wc -l
	fi
    done
done
wait

ENDTIME=$(date +%s)
Ttaken=$(($ENDTIME - $STARTTIME))
echo
echo "finish time is : $(date +"%T")"
echo "RUNTIME is :  $(($Ttaken / 3600)) hours ::  $(($(($Ttaken%3600))/60)) minutes  :: $(($Ttaken % 60)) seconds."

