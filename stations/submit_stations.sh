#!/bin/bash
#SBATCH --job-name=stations
#SBATCH --exclusive             
#SBATCH --export=ALL            
#SBATCH --nodes=1             
#SBATCH --error=stations.err            
#SBATCH --output=stations.out            
#SBATCH --ntasks-per-node=1
#SBATCH --mem=190000            
#SBATCH --time=08:00:00         

#export cwd=`pwd`                                                                
#export PYTHONPATH="$cwd/user_functions":$PYTHONPATH   
e=C201108231751A
p=T045-110s

pypaw-extract_station_info -f paths/stations.$e.path.json 
