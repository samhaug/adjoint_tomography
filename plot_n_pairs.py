#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
from pytomo3d.doubledifference.windows import component_based_windows_data

from generate_path_files import load_events
from generate_path_files import load_path_config
from generate_path_files import load_json
from generate_path_files import load_yaml
from generate_path_files import f
import generate_path_files

import numpy as np


import argparse

from collections import defaultdict
import os

from tqdm import tqdm


def get_latlon(st, stations):
    return (stations[st]["latitude"],
            stations[st]["longitude"])


def get_plot_data(pairs, stations):
    dots = {}
    lines = set()
    n_pairs = defaultdict(int)
    for pair in pairs:
        st_i = pair["window_id_i"].split(":")[0]
        st_j = pair["window_id_j"].split(":")[0]
        lat_i, lon_i = get_latlon(st_i, stations)
        lat_j, lon_j = get_latlon(st_j, stations)
        dots[".".join(st_i.split(".")[:2])] = (lat_i, lon_i)
        dots[".".join(st_j.split(".")[:2])] = (lat_j, lon_j)
        lines.add((lat_i, lon_i, lat_j, lon_j))
        n_pairs[".".join(st_i.split(".")[:2])] += 1
        n_pairs[".".join(st_j.split(".")[:2])] += 1
    return dots, lines, n_pairs


def get_window_plot_data(windows, stations):
    dots = {}
    for window in windows:
        st = window.split(":")[0]
        dots[".".join(st.split(".")[:2])] = get_latlon(st, stations)
    return dots


def plot_pairs_map(pairs, stations, windows, source_loc):
    fig = plt.figure(figsize=(12, 8))

    m = Basemap(projection='kav7', lon_0=0.0, lat_0=0.0,
                resolution='l')
    m.drawcoastlines()
    # m.drawstates()
    # m.drawcountries()
    # draw parallels.
    # parallels = np.arange(-90., 90, 10.)
    # m.drawparallels(parallels, fontsize=10)
    # draw meridians
    # meridians = np.arange(0., 360., 30.)
    # m.drawmeridians(meridians, fontsize=10)
    # m.fillcontinents(ocean_color='blue')
    # colors: http://www.color-hex.com/color-palette/9261
    # m.drawlsmask(land_color=(241, 141, 0, 100),
    #              ocean_color=(74, 128, 245, 100),
    #              lakes=True)
    m.drawlsmask(land_color=(255, 255, 255, 100),
                 ocean_color=(150, 150, 150, 100),
                 lakes=False)
    # m.drawmapboundary()

    dot_size = 100

    all_dots = get_window_plot_data(windows, stations)

    dots, lines, pairs = get_plot_data(pairs, stations)

    lats = []
    lons = []
    n_pairs = []
    for sta, (lat, lon) in all_dots.iteritems():
        lats.append(lat)
        lons.append(lon)
        n_pairs.append(pairs[sta])
    sx, sy = m(source_loc["longitude"], source_loc["latitude"])
    plt.scatter(sx, sy, dot_size*2, marker="*", color="red",
                edgecolors="k", zorder=12)
    xs, ys = m(lons, lats)
    n_pairs = [x if x == 0 else np.log(x) for x in n_pairs]
    sc = plt.scatter(xs, ys, dot_size, c=n_pairs, marker="v",
                     cmap="rainbow", edgecolors="k", zorder=11)
    plt.clim(0, 3.5)
    # cbar = plt.colorbar(sc, fraction=0.024, pad=0.02, ax=plt.gca())
    # cbar.set_label("Number of pairs (log)")

    return fig


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Plot number of pairs")
    parser.add_argument("eventname")
    parser.add_argument("period_band")
    args = parser.parse_args()

    events = load_events("event_list")
    folders, files = load_path_config("paths.yml")
    settings = load_yaml("settings.yml")

    generate_path_files.folders = folders
    generate_path_files.files = files

    eventnames = [args.eventname] if args.eventname != "*" else events
    periods = [args.period_band] if args.period_band != "*" else settings["period_bands"]

    pbar = tqdm(total=len(eventnames)*len(periods))
    for event in eventnames:
        for period in periods:
            pairs = load_json(f("dd_pairs_filter",
                                event, period))
            z_pairs = pairs["Z"]
            stations = load_json(f("stations_file",
                                   event, period))
            windows = load_json(f("windows_filter_file",
                                  event, period))
            z_windows = component_based_windows_data(windows)["Z"]
            sources = load_json("sources.json")
            source_loc = sources[event]
            figure_filename = "global_n_pairs/{}_{}_n_pairs.pdf".format(
                event, period)

            if os.path.isfile(figure_filename):
                print("File exists.")
                pbar.update()
                continue
            else:
                fig = plot_pairs_map(z_pairs, stations, z_windows, source_loc)
                plt.tight_layout()
                fig.savefig(figure_filename)
                pbar.update()
                plt.close("all")
